require("dotenv").config();
const cors = require("cors");
const PORT = process.env.PORT;
const express = require("express");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger");
const app = express();
const cookieParser = require("cookie-parser");
const http = require("http");
const socketIo = require("socket.io");
const server = http.createServer(app);
const io = socketIo(server, {
    cors: {
        origin: "http://localhost:3000",
        methods: ["GET", "POST"],
    },
});

require("./sockets/socketLogic.js")(io);

// Import Routers
const usersRouter = require("./routes/user.router");
const categoriesRouter = require("./routes/category.router");
const authenticationRouter = require("./routes/authentication.router");
const questionRouter = require("./routes/question.router");
const answerRouter = require("./routes/answer.router");
const twoFactorRouter = require("./routes/twoFactor.router");
const metamaskRouter = require("./routes/metamask.router");
const socketLogic = require("./sockets/socketLogic");
const feedbackRouter = require("./routes/feedback.router");
const authorityRouter = require("./routes/authority.router");
const rewardsRouter = require("./routes/reward.router");
const affiliationRouter = require("./routes/affiliation.router");
socketLogic(io);

console.log("Starting server.");

const corsOptions = {
    origin: "http://localhost:3000",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true,
    optionsSuccessStatus: 200,
};

// Enable CORS
app.use(cors(corsOptions));
app.use(cookieParser());

// Middleware
app.use(express.json()); // JSON body parser middleware
app.use(express.urlencoded({ extended: false })); // URL-encoded body parser middleware

// Routes
app.use("/users", usersRouter);
app.use("/categories", categoriesRouter);
app.use("/auth", authenticationRouter);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use("/questions", questionRouter);
app.use("/answers", answerRouter);
app.use("/metamask", metamaskRouter);
app.use("/twoFactor", twoFactorRouter);
app.use("/feedback", feedbackRouter);
app.use("/authority", authorityRouter);
app.use("/rewards", rewardsRouter);
app.use("/affiliation", affiliationRouter);

// Error handling middleware (optional)
app.use((err, req, res, next) => {
    console.error(err);
    res.status(err.status || 500).json({ error: err.message || "Internal Server Error" });
});

server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
