const express = require("express");
const router = express.Router();
const answerController = require("../controllers/answer.controller");
const { logger } = require("../utils/log");

/**
 * @swagger
 * tags:
 *   - name: Answers
 *     description: API endpoints for managing answers in the game's database.
 */

/**
 * @swagger
 * /answers/id/{id}:
 *   get:
 *     tags:
 *       - Answers
 *     summary: Retrieve an Answer by ID
 *     description: Retrieves a single answer using a unique identifier (ID). The ID should be a UUID.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Unique identifier of the answer to retrieve.
 *     responses:
 *       200:
 *         description: Returns a detailed answer object, including ID, content, and related question details.
 *       404:
 *         description: No answer found for the provided ID.
 *       500:
 *         description: Internal server error occurred.
 */
router.get("/id/:id", async (req, res) => {
    try {
        const answer = await answerController.getAnswer(req.params.id);
        if (!answer) {
            logger.warn("The answer with id " + req.params.id + " was requested but not found");
            return res.status(404).send("Answer not found");
        }
        logger.info("The answer with id " + req.params.id + " was requested successfully.");
        res.send(answer);
    } catch (err) {
        logger.error(
            "The answer with id " +
                req.params.id +
                " was requested but failed with error: " +
                err,
        );
        let code = 500;
        res.status(code).send(err.message);
    }
})


/**
 * @swagger
 * /answers/add:
 *   post:
 *     tags:
 *       - Answers
 *     summary: Add a New Answer
 *     description: Adds a new answer to the database. The request body must contain all required answer fields.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               content:
 *                 type: string
 *                 description: The content of the answer.
 *     responses:
 *       200:
 *         description: The newly added answer object is returned.
 *       400:
 *         description: Invalid request due to missing or incorrect answer data.
 *       409:
 *         description: Conflict occurred, possibly due to a duplicate answer entry.
 *       500:
 *         description: Internal server error occurred.
 */
router.post("/add", async (req, res) => {
    try {
        if (!req.body) {
            logger.warn("No body provided to add answer");
            return res.status(400).send("No body provided");
        }
        const answer = await answerController.createAnswer(req.body);
        if (!answer) {
            logger.warn("A answer was tried to be added but failed");
            return res.status(404).send("Bad request");
        }
        logger.info("A answer was added successfully.");
        res.send(answer);
    } catch (err) {
        logger.error("The answer was tried to be added but failed with error: " + err);
        let code = 500;
        if (err.message == "Invalid answer data") {
            code = 400;
        }
        if (err.message == "The answer is already in use") {
            code = 409;
        }
        res.status(code).send(err.message);
    }
})

/**
 * @swagger
 * /answers/question/{question_id}:
 *   get:
 *     tags:
 *       - Answers
 *     summary: Retrieve Answers for a Specific Question
 *     description: Retrieves all answers associated with a specific question using the question's unique ID.
 *     parameters:
 *       - in: path
 *         name: question_id
 *         schema:
 *           type: string
 *         required: true
 *         description: Unique identifier of the question for which answers are sought.
 *     responses:
 *       200:
 *         description: An array of answer objects related to the given question.
 *       404:
 *         description: No answers found for the provided question ID.
 *       500:
 *         description: Internal server error occurred.
 */
router.get("/question/:question_id", async (req, res) => {
    try {
        const answers = await answerController.getAnswerForQuestion(req.params.question_id);
        if (!answers) {
            logger.warn("The answers for question " + req.params.question_id + " were requested but not found");
            return res.status(404).send("Answers not found");
        }
        logger.info("The answers for question " + req.params.question_id + " were requested successfully.");
        res.send(answers);
    } catch (err) {
        logger.error(
            "The answers for question " +
                req.params.question_id +
                " were requested but failed with error: " +
                err,
        );
        res.status(500).send(err);
    }
})


/**
 * @swagger
 * /answers/addAll:
 *   post:
 *     tags:
 *       - Answers
 *     summary: Add Multiple Answers
 *     description: Adds multiple new answers to the database in a single request. The body should contain an array of answer objects.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               type: object
 *               properties:
 *                 content:
 *                   type: string
 *                   description: The content of the answer.
 *     responses:
 *       200:
 *         description: Successful addition of all provided answers.
 *       400:
 *         description: Invalid request due to incorrect answer data or format.
 *       409:
 *         description: Conflict occurred, possibly due to duplicate answers.
 *       500:
 *         description: Internal server error occurred.
 */
router.post("/addAll", async (req, res) => {
    try{
        if (!req.body) {
            logger.warn("No body provided to add answer");
            return res.status(400).send("No body provided");
        }
        const answers = await req.body;
        for (let answer of answers) {
            const response = await answerController.createAnswer(answer);
            if (!response) {
                logger.warn("A answer was tried to be added but failed");
                return res.status(404).send("Bad request");
            }
        }
        logger.info("The answers were added successfully.");
        res.send("Completed");

    }catch(err){
        logger.error("The answer was tried to be added but failed with error: " + err);
        let code = 500;
        if (err.message == "Invalid answer data") {
            code = 400;
        }
        if (err.message == "The answer is already in use") {
            code = 409;
        }
        res.status(code).send(err.message);
    }
})

module.exports = router;