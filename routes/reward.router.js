const express = require("express");
const router = express.Router();
const rewardController = require("../controllers/reward.controller");
const { logger } = require("../utils/log");
const routesUtils = require("../utils/routersUtils");


/**
 * @swagger
 * tags:
 *   - name: Rewards
 *     description: Operations related to rewards
 */

/**
 * @swagger
 * /add:
 *   post:
 *     summary: Add a new reward
 *     tags:
 *       - Rewards
 *     description: Adds a new reward to the system
 *     responses:
 *       200:
 *         description: Reward added successfully.
 *       400:
 *         description: No body provided or invalid reward data.
 *       404:
 *         description: User not found or bad request.
 *       500:
 *         description: Internal server error.
 */
router.post("/add", async (req, res) => {
    try {
        if (await routesUtils.validateToken(res, req.cookies.jwt)) return;
        if (!req.body) {
            logger.warn("No body provided to add reward");
            return res.status(400).send("No body provided");
        }
        const reward = await rewardController.createRewardCode(req.cookies.jwt);
        if (!reward) {
            logger.warn("A reward was tried to be added but failed");
            return res.status(404).send("Bad request");
        }
        logger.info("A reward was added successfully.");
        res.send(reward);
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid reward data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        logger.error("A reward was tried to be added but failed with error: " + err);
        res.status(code).send(err.message);
    }
});

/**
 * @swagger
 * /get:
 *   get:
 *     summary: Get a reward
 *     tags:
 *       - Rewards
 *     description: Retrieves a reward from the system
 *     responses:
 *       200:
 *         description: Reward retrieved successfully.
 *       400:
 *         description: Invalid reward data.
 *       404:
 *         description: User not found or bad request.
 *       500:
 *         description: Internal server error.
 */
router.get("/get", async (req, res) => {
    try {
        if (await routesUtils.validateToken(res, req.cookies.jwt)) return;
        const reward = await rewardController.getRewardCode(req.cookies.jwt);
        if (!reward) {
            logger.warn("A reward was tried to be retrieved but failed");
            return res.status(404).send("Bad request");
        }
        logger.info("A reward was retrieved successfully.");
        res.send(reward);
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid reward data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        logger.error("A reward was tried to be retrieved but failed with error: " + err);
        res.status(code).send(err.message);
    }
});


/**
 * @swagger
 * /check/{code}:
 *   get:
 *     summary: Check a reward
 *     tags:
 *       - Rewards
 *     description: Checks if a reward code exists
 *     parameters:
 *       - in: path
 *         name: code
 *         required: true
 *         schema:
 *           type: string
 *         description: The reward code to check
 *     responses:
 *       200:
 *         description: Reward exists.
 *       400:
 *         description: Invalid reward data.
 *       404:
 *         description: User not found or bad request.
 *       500:
 *         description: Internal server error.
 */
router.get("/check/:code", async (req, res) => {
    try {
        if (await routesUtils.validateToken(res, req.cookies.jwt)) return;
        const reward = await rewardController.checkIfCodeExist(req.params.code);
        if (!reward) {
            logger.warn("A reward was tried to be checked but failed");
            return res.status(404).send("Bad request");
        }
        logger.info("A reward was checked successfully.");
        res.send(reward);
    } catch (err) {checkReward
        let code = 500;
        if (err.message == "Invalid reward data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        logger.error("A reward was tried to be checked but failed with error: " + err);
        res.status(code).send(err.message);
    }
});

module.exports = router;