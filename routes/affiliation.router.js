const express = require("express");
const router = express.Router();
const affiliationController = require("../controllers/affiliation.controller");
const userController = require("../controllers/user.controller");
const { logger } = require("../utils/log");
const routesUtils = require("../utils/routersUtils");

/**
 * @swagger
 * tags:
 *   - name: Affiliation
 *     description: Operations related to affiliation codes
 */

/**
 * @swagger
 * /add:
 *   post:
 *     summary: Add an affiliation code
 *     tags:
 *       - Affiliation
 *     requestBody:
 *       description: Affiliation code to be added
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 type: string
 *                 description: The affiliation code to be added.
 *     responses:
 *       200:
 *         description: Affiliation code added successfully.
 *       400:
 *         description: No body provided or invalid affiliation code data.
 *       404:
 *         description: User not found.
 *       406:
 *         description: Cannot use own reward code.
 *       500:
 *         description: Internal server error.
 */
router.post("/add", async (req, res) => {
    try {
        if (await routesUtils.validateToken(res, req.cookies.jwt)) return;
        if (!req.body) {
            logger.warn("No body provided to add affiliation code");
            return res.status(400).send("No body provided");
        }
        await affiliationController.addAffiliationCode(req.cookies.jwt, req.body.code);
        logger.info("An affiliation code was added successfully.");
        res.send("Affiliation code added successfully");
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid affiliation code data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        if(err.message == "You can't use your own reward code"){
            code = 406;
        }
        logger.error("An affiliation code was tried to be added but failed with error: " + err);
        res.status(code).send(err.message);
    }
}
);


/**
 * @swagger
 * /get:
 *   get:
 *     summary: Get the user's affiliation code
 *     tags:
 *       - Affiliation
 *     responses:
 *       200:
 *         description: Affiliation code retrieved successfully.
 *       400:
 *         description: Invalid affiliation code data.
 *       404:
 *         description: User not found or bad request.
 *       500:
 *         description: Internal server error.
 */
router.get("/get", async (req, res) => {
    try {
        if (await routesUtils.validateToken(res, req.cookies.jwt)) return;
        const userId = await userController.getUserId(req.cookies.jwt);
        const affiliationCode = await affiliationController.getAffiliationCode(userId);
        if (!affiliationCode) {
            logger.warn("An affiliation code was tried to be retrieved but failed");
            return res.status(404).send("Bad request");
        }
        logger.info("An affiliation code was retrieved successfully.");
        res.send(affiliationCode);
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid affiliation code data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        logger.error("An affiliation code was tried to be retrieved but failed with error: " + err);
        res.status(code).send(err.message);
    }
});


module.exports = router;