const express = require("express");
const router = express.Router();
const feedbackController = require("../controllers/feedback.controller");
const { logger } = require("../utils/log");
const routesUtils = require("../utils/routersUtils");

/**
 * @swagger
 * tags:
 *   - name: Feedback
 *     description: API endpoints for managing user feedback in the system.
 */

/**
 * @swagger
 * /feedback/add:
 *   post:
 *     tags:
 *       - Feedback
 *     summary: Add New Feedback
 *     description: Allows a user to submit new feedback. The request body must contain the feedback content.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               content:
 *                 type: string
 *                 description: Content of the user's feedback.
 *     responses:
 *       200:
 *         description: Successfully added feedback, returns the added feedback object.
 *       400:
 *         description: Invalid request, feedback data missing or malformed.
 *       404:
 *         description: User associated with the feedback not found.
 *       500:
 *         description: Internal server error, unable to process request.
 */
router.post("/add", async (req, res) => {
    try {
        if (await routesUtils.validateToken(res, req.cookies.jwt)) return;
        if (!req.body) {
            logger.warn("No body provided to add feedback");
            return res.status(400).send("No body provided");
        }
        const feedback = await feedbackController.addFeedback(req.body,req.cookies.jwt);
        if (!feedback) {
            logger.warn("A feedback was tried to be added but failed");
            return res.status(404).send("Bad request");
        }
        logger.info("A feedback was added successfully.");
        res.send(feedback);
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid feedback data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        logger.error("A feedback was tried to be added but failed with error: " + err);
        res.status(code).send(err.message);
    }
});

/**
 * @swagger
 * /feedback/all:
 *   get:
 *     tags:
 *       - Feedback
 *     summary: Retrieve Feedback with Pagination
 *     description: Retrieves feedbacks from the database, supporting pagination.
 *     parameters:
 *       - in: query
 *         name: pageNumber
 *         schema:
 *           type: integer
 *         description: Page number for pagination, defaults to 1 if not provided.
 *       - in: query
 *         name: itemsPerPage
 *         schema:
 *           type: integer
 *         description: Number of items per page, defaults to 10 if not provided.
 *     responses:
 *       200:
 *         description: An array of feedback objects for the specified page.
 *       400:
 *         description: Invalid pagination parameters provided.
 *       500:
 *         description: Internal server error, unable to retrieve feedbacks.
 */
router.get("/all", async (req, res) => {
    const pageNumber = parseInt(req.query.pageNumber) || 1;
    const itemsPerPage = parseInt(req.query.itemsPerPage) || 10;

    try {
        if (pageNumber < 1 || itemsPerPage < 1) {
            throw new Error("Invalid feedback data");
        }
        if (await routesUtils.validateToken(res, req.cookies.jwt)) return;
        if (await routesUtils.validateAuthority(res,req.cookies.jwt)) return;
        const feedbacks = await feedbackController.getFeedbacks(pageNumber, itemsPerPage);
        logger.info("The feedbacks were requested successfully.");
        res.json(feedbacks);
    } catch (error) {
        let code = 500;
        if (error.message == "Invalid feedback data") {
            code = 400;
        }
        res.status(code).send(error.message);
    }
});

/**
 * @swagger
 * /feedback/{id}:
 *   get:
 *     tags:
 *       - Feedback
 *     summary: Retrieve Feedback by ID
 *     description: Retrieves a specific feedback item using its unique ID.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Unique identifier of the feedback item.
 *     responses:
 *       200:
 *         description: A detailed feedback object.
 *       404:
 *         description: Feedback item not found for the given ID.
 *       500:
 *         description: Internal server error, unable to retrieve feedback.
 */
router.get("/feedback/:id", async (req, res) => {
    try {
        if(!req.params.id){
            logger.warn("A feedback was requested but failed");
            return res.status(400).send("No id provided");
        }
        if (await routesUtils.validateToken(res, req.cookies.jwt)) return;
        if (await routesUtils.validateAuthority(res,req.cookies.jwt)) return;
        const feedback = await getFeedback(req.params.id);
        if (!feedback) {
            logger.warn("The feedback with id " + req.params.id + " was requested but not found");
            return res.status(404).send("Feedback not found");
        }
        logger.info("The feedback with id " + req.params.id + " was requested successfully.");
        res.send(feedback);

    } catch (err) {
        logger.error(
            "The feedback with id " +
                req.params.id +
                " was requested but failed with error: " +
                err,
        );
        res.status(500).send(err);
    }
});

/**
 * @swagger
 * /feedback/{id}:
 *   delete:
 *     tags:
 *       - Feedback
 *     summary: Delete Feedback by ID
 *     description: Deletes a specific feedback item from the database using its ID.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the feedback item to be deleted.
 *     responses:
 *       200:
 *         description: Successfully deleted the feedback item.
 *       404:
 *         description: Feedback item not found for the specified ID.
 *       500:
 *         description: Internal server error, unable to delete feedback.
 */
router.delete("/delete/:id", async (req, res) => {
    try {
        if(!req.params.id){
            logger.warn("A feedback was requested but failed");
            return res.status(400).send("No id provided");
        }
        if (await routesUtils.validateToken(res, req.cookies.jwt)) return;
        if (await routesUtils.validateAuthority(res,req.cookies.jwt)) return;
        const feedback = await feedbackController.deleteFeedback(req.params.id);
        if (!feedback) {
            logger.warn("The feedback with id " + req.params.id + " was requested but not found");
            return res.status(404).send("Feedback not found");
        }
        logger.info("The feedback with id " + req.params.id + " was requested successfully.");
        res.send(feedback);
    } catch (err) {
        logger.error(
            "The feedback with id " +
                req.params.id +
                " was requested but failed with error: " +
                err,
        );
        res.status(500).send(err);
    }
});


/**
 * @swagger
 * /feedback/{id}:
 *   put:
 *     tags:
 *       - Feedback
 *     summary: Update Feedback by ID
 *     description: Updates a specific feedback item using its ID. The request body must contain the updated feedback content.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the feedback item to be updated.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               content:
 *                 type: string
 *                 description: Updated content of the feedback.
 *     responses:
 *       200:
 *         description: Successfully updated the feedback item, returns the updated feedback object.
 *       404:
 *         description: Feedback item not found for the given ID.
 *       500:
 *         description: Internal server error, unable to update feedback.
 */
router.put("/update/:id", async (req, res) => {
    try {
        const feedback = await updateFeedback(req.params.id, req.body);
        if (!feedback) {
            logger.warn("The feedback with id " + req.params.id + " was requested but not found");
            return res.status(404).send("Feedback not found");
        }
        logger.info("The feedback with id " + req.params.id + " was requested successfully.");
        res.send(feedback);
    } catch (err) {
        logger.error(
            "The feedback with id " +
                req.params.id +
                " was requested but failed with error: " +
                err,
        );
        res.status(500).send(err);
    }
});

/**
 * @swagger
 * /feedbacks/{userName}:
 *   get:
 *     tags:
 *       - Feedback
 *     summary: Retrieve Feedback by User
 *     description: Retrieves all feedback submitted by a specific user, supporting pagination.
 *     parameters:
 *       - in: path
 *         name: userName
 *         schema:
 *           type: string
 *         required: true
 *         description: Username whose feedbacks are to be retrieved.
 *       - in: query
 *         name: pageNumber
 *         schema:
 *           type: integer
 *         description: Page number for pagination.
 *       - in: query
 *         name: itemsPerPage
 *         schema:
 *           type: integer
 *         description: Number of items per page.
 *     responses:
 *       200:
 *         description: An array of feedback objects submitted by the specified user.
 *       400:
 *         description: Invalid user data or pagination parameters.
 *       404:
 *         description: User not found or no feedbacks available for the user.
 *       500:
 *         description: Internal server error, unable to retrieve feedbacks.
 */
router.get("/byUserName/:userName", async (req, res) => {
    const userName = req.params.userName;
    const pageNumber = parseInt(req.query.pageNumber) || 1; // Default to page 1
    const itemsPerPage = parseInt(req.query.itemsPerPage) || 10; // Default to 10 items per page
    try {
        if (pageNumber < 1 || itemsPerPage < 1) {
            throw new Error("Invalid user data");
        }
        if (await routesUtils.validateToken(res, req.cookies.jwt)) return;
        if (await routesUtils.validateAuthority(res,req.cookies.jwt)) return;
        const feedbacks = await getAllFeedbacksByUser(userName, pageNumber, itemsPerPage);
        logger.info("The feedbacks were requested successfully.");
        res.send(feedbacks);
    } catch (error) {
        let code = 500;
        if (error.message == "Invalid user data") {
            code = 400;
        }
        if (error.message == "User not found") {
            code = 404;
        }
        logger.error("The feedbacks were requested but failed with error: " + error);
        res.status(code).send(error.message);
    }
});

module.exports = router;
