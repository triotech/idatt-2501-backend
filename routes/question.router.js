const express = require("express");
const router = express.Router();
const questionController = require("../controllers/question.controller");
const categoryController = require("../controllers/category.controller");
const answerController = require("../controllers/answer.controller");
const { logger } = require("../utils/log");
const routersUtils = require("../utils/routersUtils");

/**
 * @swagger
 * tags:
 *   - name: Questions
 *     description: Operations related to questions
 */

/**
 * @swagger
 * /questions/id/{id}:
 *   get:
 *     summary: Retrieve a question by ID
 *     tags:
 *       - Questions
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: The ID of the question to retrieve
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: The question is found and returned
 *       404:
 *         description: The question with the specified ID is not found
 *       500:
 *         description: Internal server error
 */
router.get("/id/:id", async (req, res) => {
    try {
        const question = await questionController.getQuestion(req.params.id);
        if (!question) {
            logger.warn("The question with id " + req.params.id + " was requested but not found");
            return res.status(404).send("Question not found");
        }
        logger.info("The question with id " + req.params.id + " was requested successfully.");
        res.send(question);
    } catch (err) {
        logger.error(
            "The question with id " +
                req.params.id +
                " was requested but failed with error: " +
                err,
        );
        res.status(500).send(err);
    }
});

/**
 * @swagger
 * /questions/add:
 *   post:
 *     summary: Add a new question
 *     tags:
 *       - Questions
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               text:
 *                 type: string
 *                 description: The text of the question.
 *               categories:
 *                 type: array
 *                 items:
 *                   type: string
 *                 description: An array of category IDs.
 *               difficulty:
 *                 type: integer
 *                 description: The difficulty level of the question.
 *     responses:
 *       200:
 *         description: The question was added successfully
 *       400:
 *         description: Bad request, invalid question data or no body provided
 *       404:
 *         description: Category not found
 *       409:
 *         description: The question was already in use
 *       500:
 *         description: Internal server error
 */

router.post("/add", async (req, res) => {
    try {
        if (!req.body) {
            logger.warn("No body provided to add question");
            return res.status(400).send("No body provided");
        }
        const question = await questionController.createQuestion(req.body);
        if (!question) {
            logger.warn("A question was tried to be added but failed");
            return res.status(404).send("Bad request");
        }
        logger.info("A question was added successfully.");
        res.send(question);
    } catch (err) {
        logger.error("The question was tried to be added but failed with error: " + err);
        let code = 500;
        if (err.message == "Invalid question data") {
            code = 400;
        }
        if (err.message == "The question is already in use") {
            code = 409;
        }
        if (err.message == "Category not found") {
            code = 404;
        }
        res.status(code).send(err.message);
    }
});

/**
 * @swagger
 * /questions/addAll:
 *   post:
 *     summary: Add multiple new questions
 *     tags:
 *       - Questions
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               type: object
 *               properties:
 *                 text:
 *                   type: string
 *                   description: The text of the question.
 *                 categories:
 *                   type: array
 *                   items:
 *                     type: string
 *                   description: An array of category IDs.
 *                 difficulty:
 *                   type: integer
 *                   description: The difficulty level of the question.
 *     responses:
 *       200:
 *         description: All questions are added successfully
 *       400:
 *         description: Bad request, invalid question data or no body provided
 *       404:
 *         description: Category not found
 *       409:
 *         description: A question is already in use
 *       500:
 *         description: Internal server error
 */
router.post("/addAll", async (req, res) => {
    try {
        if (!req.body) {
            logger.warn("No body provided to add question");
            return res.status(400).send("No body provided");
        }
        const questions = await req.body;

        for (let question of questions) {
            const response = await questionController.createQuestion(question);
            if (!response) {
                logger.warn("A question was tried to be added but failed");
                return res.status(404).send("Bad request");
            }
        }
        logger.info("The questions were added successfully.");
        res.send("Completed");
    } catch (err) {
        logger.error("The question was tried to be added but failed with error: " + err);
        let code = 500;
        if (err.message == "Invalid question data") {
            code = 400;
        }
        if (err.message == "The question is already in use") {
            code = 409;
        }
        if (err.message == "Category not found") {
            code = 404;
        }
        res.status(code).send(err.message);
    }
});


/**
 * @swagger
 * /addQuestion:
 *   post:
 *     summary: Add a new question
 *     tags:
 *       - Questions
 *     description: Adds a new question to the database with its associated answers and metadata
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - question
 *               - category
 *               - difficulty
 *               - answers
 *               - funFact
 *               - correctAnswer
 *             properties:
 *               question:
 *                 type: string
 *                 description: The text of the question
 *               category:
 *                 type: string
 *                 description: The category of the question
 *               difficulty:
 *                 type: integer
 *                 description: The difficulty level of the question
 *               answers:
 *                 type: array
 *                 items:
 *                   type: string
 *                 description: The list of answers
 *               funFact:
 *                 type: string
 *                 description: A fun fact related to the question
 *               correctAnswer:
 *                 type: integer
 *                 description: The index of the correct answer in the answers array
 *     responses:
 *       200:
 *         description: Successfully added question.
 *       400:
 *         description: Invalid request data.
 *       404:
 *         description: Category not found.
 *       409:
 *         description: The question is already in use.
 *       500:
 *         description: Internal server error.
 */
router.post("/addQuestion", async (req, res) => {
    try{
        if(await routersUtils.validateToken(res, req.cookies.jwt)) return;
        if(await routersUtils.validateAuthority(res, req.cookies.jwt)) return;
        if(!req.body || !req.body.question || !req.body.category || req.body.difficulty === undefined || !req.body.answers || !req.body.funFact || req.body.correctAnswer === undefined){
            logger.warn("No body provided to add question");
            return res.status(400).send("No body provided");
        }
        await questionController.createQuestion({"text": req.body.question, "categories": [req.body.category], "difficulty": req.body.difficulty, "fact": req.body.funFact});
        const question = await questionController.getQuestionByText(req.body.question);
        const answers = req.body.answers;
        const correct = req.body.correctAnswer;
        for(let i = 0; i < answers.length; i++){
            let correct_answer = false;
            if(i === correct){
                correct_answer = true;
            }
            await answerController.createAnswer({"text": answers[i], "question_id": question._id, "is_correct": correct_answer});
        }

        logger.info("A question was added successfully.");
        res.status(200).send("Successfully added question");
    }catch(err){
        logger.error("The question was tried to be added but failed with error: " + err);
        let code = 500;
        if (err.message == "Invalid question data") {
            code = 400;
        }
        if (err.message == "The question is already in use") {
            code = 409;
        }
        if (err.message == "Category not found") {
            code = 404;
        }
        res.status(code).send(err.message);
    }

});

module.exports = router;
