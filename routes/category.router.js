const express = require("express");
const router = express.Router();
const categoryController = require("../controllers/category.controller");
const { logger } = require("../utils/log");
const routersUtils = require("../utils/routersUtils");

/**
 * @swagger
 * tags:
 *   - name: Categories
 *     description: Operations related to categories
 */

/**
 * @swagger
 * /categories/id/{id}:
 *   get:
 *     summary: Retrieve a category by its ID
 *     tags:
 *      - Categories
 *     description: Retrieves a single category based on the ID provided.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The ID of the category to retrieve.
 *     responses:
 *       200:
 *         description: A category object.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 name:
 *                   type: string
 *                 description:
 *                   type: string
 *       404:
 *         description: Category not found.
 *       500:
 *         description: Server error.
 */
router.get("/id/:id", async (req, res) => {
    try {
        const category = await categoryController.getCategory(req.params.id);
        if (!category) {
            logger.warn("The category with id " + req.params.id + " was requested but not found");
            return res.status(404).send("Category not found");
        }
        logger.info("The category with id " + req.params.id + " was requested successfully.");
        res.send(category);
    } catch (err) {
        logger.error(
            "The category with id " +
                req.params.id +
                " was requested but failed with error: " +
                err,
        );
        res.status(500).send(err);
    }
});

/**
 * @swagger
 * /categories/add:
 *   post:
 *     tags:
 *      - Categories
 *     summary: Add a new category
 *     description: Adds a new category to the database.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *     responses:
 *       200:
 *         description: The added category object.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 name:
 *                   type: string
 *                 description:
 *                   type: string
 *       400:
 *         description: Bad Request, no body provided.
 *       404:
 *         description: Bad Request, category not added.
 *       500:
 *         description: Server error.
 */
router.post("/add", async (req, res) => {
    try {
        if (!req.body) {
            logger.warn("No body provided to add user");
            return res.status(400).send("No body provided");
        }
        const category = await categoryController.createCategory(req.body);
        if (!category) {
            logger.warn("The category with id " + req.params.id + " was requested but not added");
            return res.status(404).send("Bad request");
        }
        logger.info("The category was added successfully.");
        res.send(category);
    } catch (err) {
        logger.error("A category was tried to be added but failed with error: " + err);
        res.status(500).send(err);
    }
});

/**
 * @swagger
 * /categories/all:
 *   get:
 *     summary: Retrieve all categories
 *     tags:
 *     - Categories
 *     description: Retrieves a list of all categories from the database.
 *     responses:
 *       200:
 *         description: An array of categories.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   name:
 *                     type: string
 *                   description:
 *                     type: string
 *       500:
 *         description: Server error.
 */
router.get("/all", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) return;
        if (await routersUtils.validateAuthority(res, req.cookies.jwt)) return;
        const categories = await categoryController.getAllCategories();
        logger.info("All categories were requested successfully.");
        res.send(categories);
    } catch (err) {
        logger.error("All categories were requested but failed with error: " + err);
        res.status(500).send(err);
    }
});

module.exports = router;
