const express = require("express");
const router = express.Router();
const metamaskController = require("../controllers/metamask.controller");
const { logger } = require("../utils/log");
const { interact } = require("../utils/web3");
const { addToQueue } = require("../utils/web3queue");

/**
 * @swagger
 * tags:
 *   - name: Metamask
 *     description: Operations related to MetaMask
 */

/**
 * @swagger
 * /metamask/auth:
 *   post:
 *     summary: Authenticate user with MetaMask
 *     tags:
 *       - Metamask
 *     description: Authenticates the user using a MetaMask signature and additional context
 *     requestBody:
 *       required: true
 *     responses:
 *       200:
 *         description: User authenticated successfully.
 *       400:
 *         description: Invalid request data or signature.
 *       500:
 *         description: Internal server error
 */
router.post("/auth", async (req, res) => {
    try {
        const { signature, fingerprint, timestamp, username } = req.body;
        if (!signature || !fingerprint || !timestamp || !username) {
            return res.status(400).send("Invalid request data");
        }
        const data = await metamaskController.metamaskOneClickLogin({ signature, fingerprint, timestamp, username });
        const jwt = data.jwt_token;
        
        const extractedID = await metamaskController.extractIdFromJWT(jwt);

        addToQueue(extractedID, async () => {
            await interact(extractedID);
        });

        logger.info("User " + username + " authenticated successfully with MetaMask");
        res.cookie("jwt", jwt, {
            httpOnly: true,
            secure: false,
            sameSite: "strict",
            path: "/",
            maxAge: 60 * 60 * 1000 * 24,
        });

        
        res.status(200).send(data.user);
    } catch (err) {
        let code = 500;
        if (err.message === "Invalid request data" || err.message === "Invalid signature") {
            code = 400;
        }

        logger.error("Error while authenticating user with MetaMask: " + err.message);

        res.status(code).send(err.message);
    }
});

/**
 * @swagger
 * /metamask/address:
 *   get:
 *     summary: Get the address from JWT
 *     tags:
 *       - Metamask
 *     description: Retrieves the address associated with the provided JWT
 *     responses:
 *       200:
 *         description: Address retrieved successfully.
 *       403:
 *         description: No JWT provided.
 *       500:
 *         description: Internal server error
 */
router.get("/address", async (req, res) => { 
    const token = req.cookies.jwt;

    if (!token) {
        return res.status(403).send("No JWT provided");
    }

    try {
    const response = await metamaskController.extractIdFromJWT(token);
    logger.info("Address extracted successfully from MetaMaskJWT");
    res.status(200).send(response);

    } catch (err) {
        logger.error("Error while extracting address from MetaMaskJWT: " + err.message);
        return res.status(500).send(err.message);
    }
});

module.exports = router;
