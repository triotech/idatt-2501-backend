const express = require("express");
const router = express.Router();
const twoFactorController = require("../controllers/twoFactor.controller");
const { logger } = require("../utils/log");
const { verify } = require("jsonwebtoken");
const routersUtils = require("../utils/routersUtils");

/**
 * @swagger
 * tags:
 *   - name: TwoFactor
 *     description: Operations related to two-factor authentication
 */

/**
 * @swagger
 * /add:
 *   post:
 *     summary: Add two-factor authentication for a user
 *     tags:
 *       - TwoFactor
 *     requestBody:
 *       description: Data required to add two-factor authentication
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The name of the user for whom to enable two-factor authentication.
 *     responses:
 *       200:
 *         description: Two-factor authentication was added successfully.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *       400:
 *         description: Bad request, invalid two-factor data or no body provided.
 *       404:
 *         description: User not found.
 *       500:
 *         description: Internal server error.
 */
router.post("/add", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) return;
        if(await routersUtils.isMetamaskUser(res, req.cookies.jwt)) return;
        const twoFactor = await twoFactorController.addTwoFactor(req.cookies.jwt);
        if (!twoFactor) {
            logger.warn("A two factor was tried to be added but failed");
            return res.status(404).send("Bad request");
        }
        logger.info("A two factor was added successfully.");
        res.status(200).json(twoFactor);
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid two factor data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        logger.error("The two factor was tried to be added but failed with error: " + err.message);
        res.send(err.message).status(code);
        
    }
});


/**
 * @swagger
 * /has2fa:
 *   get:
 *     summary: Check if user has two-factor authentication enabled
 *     tags:
 *       - TwoFactor
 *     responses:
 *       200:
 *         description: Returns true if two-factor authentication is enabled, false otherwise.
 *       400:
 *         description: Invalid data provided.
 *       404:
 *         description: User not found.
 *       500:
 *         description: Internal server error.
 */
router.get("/has2fa", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) return;
        if(await routersUtils.isMetamaskUser(res, req.cookies.jwt)) return;
        if(await twoFactorController.hasTwoFactorEnabled(req.cookies.jwt)){
            logger.info("The user has two factor enabled");
            res.status(200).send("true");
        }
        else{
            logger.info("The user does not have two factor enabled");
            res.status(200).send("false");
        }
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid two factor data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        logger.error("The two factor was tried to be verified but failed with error: " + err.message);
        res.send(err.message).status(code);
    }
}
);

/**
 * @swagger
 * /delete:
 *   delete:
 *     summary: Delete two-factor authentication for a user
 *     tags:
 *       - TwoFactor
 *     responses:
 *       200:
 *         description: Two-factor authentication deleted successfully.
 *       400:
 *         description: Invalid two-factor data.
 *       404:
 *         description: User not found.
 *       500:
 *         description: Internal server error.
 */
router.delete("/delete", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) return;
        if(await routersUtils.isMetamaskUser(res, req.cookies.jwt)) return;
        res.status(200).json(await twoFactorController.removeTwoFactor(req.cookies.jwt));
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid two factor data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        logger.error("The two factor was tried to be deleted but failed with error: " + err.message);
        res.send(err.message).status(code);
    }
}
);

/**
 * @swagger
 * /verify:
 *   post:
 *     summary: Verify two-factor authentication for a user
 *     tags:
 *       - TwoFactor
 *     requestBody:
 *       description: Data required to verify two-factor authentication
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               input:
 *                 type: string
 *                 description: The two-factor authentication input code.
 *               name:
 *                 type: string
 *                 description: The name of the user.
 *     responses:
 *       200:
 *         description: Returns true if verification is successful, false otherwise.
 *       400:
 *         description: Invalid data provided.
 *       404:
 *         description: User not found.
 *       500:
 *         description: Internal server error.
 */
router.post("/verify", async (req, res) => {
    try {
        if(!req.body.input){
            logger.warn("No input provided to verify two factor");
            return res.status(400).send("No input provided");
        }
        if (!req.body.name) {
            logger.warn("No user provided to verify two factor");
            return res.status(400).send("No user provided");
        }
        if(await twoFactorController.verifyTwoFactor(req.body.name, req.body.input)){
            logger.info("The user has successfully verified two factor");
            res.status(200).send("true");
        }
        else{
            logger.info("The user has failed to verify two factor");
            res.status(200).send("false");
        }
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid two factor data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        logger.error("The two factor was tried to be verified but failed with error: " + err.message);
        res.send(err.message).status(code);
    }
}
);

module.exports = router;
