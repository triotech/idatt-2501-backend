const express = require("express");
const router = express.Router();
const authorityController = require("../controllers/authority.controller");
const { logger } = require("../utils/log");
const routersUtils = require("../utils/routersUtils");


/**
 * @swagger
 * tags:
 *   - name: Affiliation
 *     description: Operations related to affiliation codes
 */

/**
 * @swagger
 * /add:
 *   post:
 *     summary: Add an affiliation code
 *     tags:
 *       - Affiliation
 *     requestBody:
 *       description: Affiliation code to be added
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 type: string
 *                 description: The affiliation code to be added.
 *     responses:
 *       200:
 *         description: Affiliation code added successfully.
 *       400:
 *         description: No body provided or invalid affiliation code data.
 *       404:
 *         description: User not found.
 *       406:
 *         description: Cannot use own reward code.
 *       500:
 *         description: Internal server error.
 */
router.get("/get", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) {
            return;
        }
        if(await routersUtils.isMetamaskUser(res, req.cookies.jwt)) return;
        const authority = await authorityController.getAuthority(req.cookies.jwt);
        if (!authority) {
            logger.warn("The authority was requested but not found");
            return res.status(404).send("Authority not found");
        }
        logger.info("The authority was requested successfully.");
        res.send(authority);
    } catch (err) {
        logger.error("The authority was requested but failed with error: " + err);
        res.status(500).send(err);
    }
})


/**
 * @swagger
 * /get:
 *   get:
 *     summary: Get the user's affiliation code
 *     tags:
 *       - Affiliation
 *     responses:
 *       200:
 *         description: Affiliation code retrieved successfully.
 *       400:
 *         description: Invalid affiliation code data.
 *       404:
 *         description: User not found or bad request.
 *       500:
 *         description: Internal server error.
 */
router.put("/setAdmin", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) {
            return;
        }
        if (await routersUtils.validateAuthority(res, req.cookies.jwt)) {
            return;
        }
        if(await routersUtils.isMetamaskUser(res, req.cookies.jwt)) return;
        if(!req.body.username){
            logger.warn("No body provided to set admin");
            return res.status(400).send("No body provided");
        }
        await authorityController.changeAuthority(req.body.username, "admin");
        logger.info("The authority was changed successfully.");
        res.send("Authority changed successfully");
    } catch (err) {
        logger.error("The authority was changed but failed with error: " + err);
        res.status(500).send(err);
    }
})

module.exports = router;