const express = require("express");
const router = express.Router();
const authenthicationController = require("../controllers/authentication.controller");
const twoFactorController = require("../controllers/twoFactor.controller");
const { logger } = require("../utils/log");
const { verifyJWT } = require("../utils/verifyJWT");

/**
 * @swagger
 * tags:
 *   - name: Authentication
 *     description: Api endpoints for authentication
 */

/**
 * @swagger
 * /auth/authenticate:
 *   post:
 *     summary: Authenticate user and return response
 *     tags:
 *       - Authentication
 *     requestBody:
 *       description: User data for authentication
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               password:
 *                 type: string
 *     responses:
 *       200:
 *         description: Successful authentication
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *       400:
 *         description: Bad request or Invalid authentication data
 *       401:
 *         description: Invalid password
 *       404:
 *         description: User not found
 *       500:
 *         description: Internal server error
 */
router.post("/authenticate", async (req, res) => {
    try {
        if (!req.body || !req.body.name || !req.body.password) {
            logger.warn("No body provided to authenticate user");
            return res.status(400).send("No body provided");
        }

        const twoFactor = await twoFactorController.hasTwoFactorEnabledByName(req.body.name);
        const data = await authenthicationController.authenticateUser(req.body);

        if(twoFactor){
            if(!req.body.code){
                logger.warn("No code provided to authenticate user");
                return res.status(400).send("No code provided");
            }
            const response = await twoFactorController.verifyTwoFactor(req.body.name, req.body.code);
            if(!response){
                logger.warn("Invalid code provided to authenticate user");
                return res.status(400).send("Invalid code provided");
            }
        }

        logger.info("A user was authenticated successfully.");
        res.send(data);
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid authentication data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        if (err.message == "Invalid password") {
            code = 400;
        }
        logger.error("The user was tried to be added but failed with error: " + err.message);
        res.status(code).send(err.message);
    }
});

/**
 * @swagger
 * /auth/generateJWT:
 *   post:
 *     summary: Generate a JWT token
 *     tags:
 *       - Authentication
 *     description: Generates a JWT token.
 *     parameters:
 *       - in: header
 *         name: Authorization
 *         schema:
 *           type: string
 *         required: true
 *         description: Authorization header containing the bearer token.
 *     requestBody:
 *       description: Data required to generate JWT
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               fingerprint:
 *                 type: string
 *     responses:
 *       200:
 *         description: Successfully generated JWT
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 jwt:
 *                   type: string
 *       400:
 *         description: Bad request, missing user_id or fingerprint
 *       500:
 *         description: Internal server error
 */
router.post("/generateJWT", async (req, res) => {
    if (!req.body || !req.body.fingerprint) {
        logger.warn("No body provided to generate JWT");
        return res.status(400).send("No body provided");
    }
    try {
        const token = req.cookies.jwt;

        const data = await authenthicationController.generateJWT(req.body.fingerprint, token);
        logger.info("A JWT was generated successfully.");

        res.cookie("jwt", data, {
            httpOnly: true,
            secure: false, 
            sameSite: "strict",
            path: "/",
            maxAge: 60 * 60 * 1000,
        });
        res.status(200).send("Set cookie");
    } catch (err) {
        let code = 500;
        if (err.message == "Invalid user data") {
            code = 400;
        }
        if (err.message == "Invalid refresh token") {
            code = 401;
        }
        logger.error("The refreshing of token failed with the error: " + err.message);
        res.status(code).send(err.message);
    }
});

/**
 * @swagger
 * /auth/set-cookie:
 *   post:
 *     summary: Set a JWT cookie
 *     tags:
 *       - Authentication
 *     requestBody:
 *       description: JWT token to set as a cookie
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               token:
 *                 type: string
 *     responses:
 *       200:
 *         description: Successfully set the cookie
 *       400:
 *         description: Bad request, missing token
 *       500:
 *         description: Internal server error
 */
router.post("/set-cookie", async (req, res) => {
    res.cookie("jwt", req.body.token, {
        httpOnly: true,
        secure: false,
        sameSite: "strict",
        path: "/",
        maxAge: 60 * 60 * 1000,
    });
    logger.info("A JWT was set as a cookie successfully.");
    res.status(200).send("Set cookie");
});

/**
 * @swagger
 * /auth/delete-cookie:
 *   post:
 *     summary: Delete the JWT cookie
 *     tags:
 *       - Authentication
 *     responses:
 *       200:
 *         description: Successfully deleted the cookie
 *       500:
 *         description: Internal server error
 */
router.post("/delete-cookie", async (req, res) => {
    res.clearCookie("jwt");
    logger.info("A JWT was deleted successfully.");
    res.status(200).send("Deleted cookie");
});

/**
 * @swagger
 * /auth/validateToken:
 *   get:
 *     summary: Validates a JWT token based on cookie data
 *     tags:
 *       - Authentication
 *     responses:
 *       200:
 *         description: Successfully validated JWT
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 user_id:
 *                   type: string
 *       400:
 *         description: Bad request, no JWT provided
 *       500:
 *         description: Internal server error, JWT validation failed
 */
router.get("/validateToken", async (req, res) => {
    const jwt = req.cookies.jwt;
    if (!jwt) {
        logger.warn("No JWT provided");
        return res.status(400).send("No JWT provided");
    }
    try {
        const data = await verifyJWT(jwt);
        logger.info("A JWT was validated successfully.");
        res.send(data);
    } catch (err) {
        logger.error("The JWT was tried to be validated but failed with error: " + err.message);
        res.status(401).send(err.message);
    }
});

module.exports = router;
