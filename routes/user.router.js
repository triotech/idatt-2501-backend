const express = require("express");
const router = express.Router();
const usersController = require("../controllers/user.controller");
const metamaskController = require("../controllers/metamask.controller");
const { logger } = require("../utils/log");
const { verifyJWT } = require("../utils/verifyJWT");
const fs = require("fs");
const path = require("path");
const routersUtils = require("../utils/routersUtils");

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: API endpoints for managing users
 */

/**
 * @swagger
 * /users/id/{name}:
 *   get:
 *     tags:
 *       - Users
 *     summary: Retrieve a user by ID
 *     description: >
 *       Retrieve detailed information about a user by their unique identifier
 *       Returns a 404 status if the user is not found
 *     operationId: getUserById
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: The unique identifier of the user
 *     responses:
 *       200:
 *         description: A user object containing detailed information
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                   type: string
 *                 name:
 *                   type: string
 *                 email:
 *                   type: string
 *       404:
 *         description: User not found.
 *       500:
 *         description: Internal server error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 */
router.get("/id/:name", async (req, res) => {
    try {
        if(req.params.name.includes("@")){
            res.send({
                name: req.params.name,
                country: "ETH",
            })
            return;
        }
        const user = await usersController.getUser(req.params.name);
        if (!user) {
            logger.warn("The user with id " + req.params.name + " was requested but not found");
            return res.status(404).send("User not found");
        }
        logger.info("The user with id " + req.params.name + " was requested successfully.");
        res.send(user);
    } catch (err) {
        logger.error(
            "The user with id " + req.params.name + " was requested but failed with error: " + err,
        );
        res.status(500).send(err);
    }
});

/**
 * @swagger
 * /users/add:
 *   post:
 *     tags:
 *       - Users
 *     summary: Add a new user
 *     description: >
 *       Adds a new user to the database. Returns 400 status if no body is provided,
 *       and 404 status if the user could not be created
 *     operationId: addUser
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *     responses:
 *       200:
 *         description: The added user object
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                   type: string
 *                 name:
 *                   type: string
 *                 email:
 *                   type: string
 *       400:
 *         description: Bad Request, no body provided
 *       404:
 *         description: Bad Request, user could not be created
 *       500:
 *         description: Internal Server Error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 */
router.post("/add", async (req, res) => {
    try {
        if (!req.body) {
            logger.warn("No body provided to add user");
            return res.status(400).send("No body provided");
        }
        const user = await usersController.createUser(req.body);
        if (!user) {
            logger.warn("A user was tried to be added but failed");
            return res.status(404).send("Bad request");
        }
        logger.info("A user was added successfully.");
        res.send(user);
    } catch (err) {
        logger.error("The user was tried to be added but failed with error: " + err);
        let code = 500;
        if (err.message == "Invalid user data") {
            code = 400;
        }
        if (err.message == "The email is already in use" || err.message == "The name is already in use") {
            code = 409;
        }

        res.status(code).send(err.message);
    }
});

/**
 * @swagger
 * /users/image/{name}:
 *   get:
 *     summary: Retrieve the user's image by username
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: name
 *         required: true
 *         description: The username of the user whose image is to be retrieved
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Successfully retrieved the user's image
 *       400:
 *         description: Bad request, no name provided
 *       401:
 *         description: Unauthorized, invalid or no token provided
 *       500:
 *         description: Internal server error
 */
router.get("/image/:name", async (req, res) => {
    try {
        if (!req.params.name) {
            logger.warn("No id provided to get user image");
            return res.status(400).send("No name provided");
        }
        let url;
        try {

            url = await usersController.getUserImage(req.params.name);
            url = path.resolve(__dirname, "..") + "/" + url;
        }
        catch (error){
            url = path.resolve(__dirname, "..") + "/images/MetaMask_fox.svg";
        }
        res.status(200).sendFile(url);
    } catch (err) {
        logger.error(
            "Request of the image for user " + req.params.name + " failed with error: " + err,
        );
        res.status(500).send(err);
    }
});

/**
 * @swagger
 * /users/style/{style}:
 *   put:
 *     summary: Change the user's style setting
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: style
 *         required: true
 *         description: The new style setting for the user
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Successfully changed the user's style setting
 *       400:
 *         description: Bad request, no style provided
 *       401:
 *         description: Unauthorized, no token provided
 *       500:
 *         description: Internal server error
 */
router.put("/style", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) return;
        if(await routersUtils.isMetamaskUser(res, req.cookies.jwt)) return;
        if(!req.body.style) {
            logger.warn("No style provided to change user style");
            return res.status(400).send("No style provided");
        }
        res.status(200).send(await usersController.changeStyle(req.body.style, req.cookies.jwt));
    } catch (err) {
        let code = 500;
        if (
            err.message.includes("jwt malformed") ||
            err.message.includes("invalid signature") ||
            err.message.includes("Invalid token")
        ) {
            code = 401;
            err.message = "Invalid token provided";
        }
        if (err.message == "Invalid user data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        res.status(code).send(err.message);
    }
});

/**
 * @swagger
 * /users/userId:
 *   get:
 *     summary: Retrieve the user's ID based on JWT token
 *     tags:
 *       - Users
 *     responses:
 *       200:
 *         description: Successfully retrieved the user's ID
 *       401:
 *         description: Unauthorized, invalid or no token provided
 *       500:
 *         description: Internal server error
 */
router.get("/userId", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) return;
        logger.info("User id was requested successfully.");
        let id = await usersController.getUserId(req.cookies.jwt);
        
        if ( id === undefined) {
            id = await metamaskController.extractIdFromJWT(req.cookies.jwt);
        }
        
        res.status(200).send(id);
    } catch (err) {
        logger.error("User id was requested but failed with error: " + err);
        res.status(500).send(err);
    }
});

/**
 * @swagger
 * /users/changePassword:
 *   put:
 *     summary: Change the user's password
 *     tags:
 *       - Users
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               old:
 *                 type: string
 *               updated:
 *                 type: string
 *     responses:
 *       200:
 *         description: Successfully changed password
 *       400:
 *         description: Bad request, invalid user data or invalid password
 *       404:
 *         description: User not found
 *       500:
 *         description: Internal server error
 */
router.put("/changePassword", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) return;
        if(await routersUtils.isMetamaskUser(res, req.cookies.jwt)) return;
        if (!req.body || !req.body.old || !req.body.updated) {
            logger.warn("No password provided to change user password");
            return res.status(400).send("No password provided");
        }
        logger.info("User id was requested successfully.");
        res.status(200).send(await usersController.changePassword(req.body.old, req.body.old, req.cookies.jwt));
    } catch (err) {
        logger.error("User id was requested but failed with error: " + err);
        let code = 500;
        if (err.message == "Invalid user data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        if (err.message == "Invalid password") {
            code = 400;
        }
        res.status(code).send(err);
    }
});

/**
 * @swagger
 * /users/changeAvatar:
 *   put:
 *     summary: Change the user's avatar
 *     tags:
 *       - Users
 *     responses:
 *       200:
 *         description: Successfully changed avatar
 *       400:
 *         description: Bad request, invalid user data
 *       500:
 *         description: Internal server error
 */
router.put("/changeAvatar", async (req, res) => {
    try {
        if( await routersUtils.validateToken(res, req.cookies.jwt)) return;
        if(await routersUtils.isMetamaskUser(res, req.cookies.jwt)) return;
        logger.info("User id was requested successfully.");
        res.status(200).send(await usersController.changeAvatar(req.cookies.jwt));
    } catch (err) {
        let code = 500;
        if (err.message.includes("jwt malformed") || err.message.includes("invalid signature")) {
            code = 401;
            err.message = "Invalid token provided";
        }
        logger.error("User id was requested but failed with error: " + err);
        if (err.message == "Invalid user data") {
            code = 400;
        }
        res.status(code).send(err);
    }
});

/**
 * @swagger
 * /users/activeUsers:
 *   get:
 *     summary: Retrieve the list of active users
 *     tags:
 *       - Users
 *     responses:
 *       200:
 *         description: Successfully retrieved the list of active users
 *       401:
 *         description: Unauthorized, invalid or no token provided
 *       500:
 *         description: Internal server error
 */
router.get("/activeUsers", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) return;
        logger.info("Active users were requested successfully.");
        const activeUsers = await usersController.getActiveUsers();
        res.status(200).send(""+activeUsers);
    } catch (err) {
        let code = 500;
        logger.error("Active users were requested but failed with error: " + err);
        res.status(code).send(err);
    }
});

/**
 * @swagger
 * /getUsers/{name}:
 *   get:
 *     summary: Get users by partial name
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: name
 *         required: true
 *         schema:
 *           type: string
 *         description: Partial name to search for users.
 *     responses:
 *       200:
 *         description: Users retrieved successfully.
 *       400:
 *         description: No name provided.
 *       500:
 *         description: Internal server error.
 */
router.get("/getUsers/:name", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) return;
        if(await routersUtils.isMetamaskUser(res, req.cookies.jwt)) return;
        if (await routersUtils.validateAuthority(res, req.cookies.jwt)) return;
    
        if(!req.params.name) {
            logger.warn("No name provided to get users");
            return res.status(400).send("No name provided");
        }   
        logger.info("Active users were requested successfully.");
        res.status(200).send(await usersController.getUsersByPartialName(req.params.name));
    } catch (err) {
        let code = 500;
        logger.error("Active users were requested but failed with error: " + err);
        res.status(code).send(err);
    }
});


/**
 * @swagger
 * /changeCountry:
 *   put:
 *     summary: Change the country of the current user
 *     tags:
 *       - Users
 *     requestBody:
 *       description: Country to be set for the user
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               country:
 *                 type: string
 *                 description: The new country of the user.
 *     responses:
 *       200:
 *         description: Country changed successfully.
 *       400:
 *         description: No country provided or invalid user data.
 *       404:
 *         description: User not found.
 *       500:
 *         description: Internal server error.
 */
router.put("/changeCountry", async (req, res) => {
    try {
        if (await routersUtils.validateToken(res, req.cookies.jwt)) return;
        if(await routersUtils.isMetamaskUser(res, req.cookies.jwt)) return;
        if(!req.body.country) {
            logger.warn("No country provided to change user country");
            return res.status(400).send("No country provided");
        }
        logger.info("User id was requested successfully.");
        res.status(200).send(await usersController.changeCountry(req.body.country, req.cookies.jwt));
    } catch (err) {
        logger.error("User id was requested but failed with error: " + err);
        let code = 500;
        if (err.message == "Invalid user data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        res.status(code).send(err);
    }
});

/**
 * @swagger
 * /getScoreTable:
 *   get:
 *     summary: Get the score table of users
 *     tags:
 *       - Users
 *     responses:
 *       200:
 *         description: Score table retrieved successfully.
 *       400:
 *         description: Invalid user data.
 *       404:
 *         description: User not found.
 *       500:
 *         description: Internal server error.
 */
router.get("/getScoreTable", async (req, res) => {

    try{
        if(await routersUtils.validateToken(res, req.cookies.jwt)) return;
        logger.info("Score table was requested successfully.");
        res.status(200).send(await usersController.getFormattedScoreTable(req.cookies.jwt));
    } catch (err) {
        logger.error("Score table was requested but failed with error: " + err);
        let code = 500;
        if (err.message == "Invalid user data") {
            code = 400;
        }
        if (err.message == "User not found") {
            code = 404;
        }
        res.status(code).send(err);
    }

});
    

module.exports = router;
