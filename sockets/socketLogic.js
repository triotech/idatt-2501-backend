const crypto = require("crypto");
const { Lobby } = require("../models/lobby.model");
const { logger } = require("../utils/log");
const {Game}  = require("../models/game.model");
const web3 = require("../utils/web3");
const {addToQueue , queueEmitter} = require("../utils/web3queue");
const {getAffiliationCode} = require("../controllers/affiliation.controller");
const {getUserIdByRewardCode} = require("../controllers/reward.controller");


/**
 * An array to store lobbies.
 * @type {Lobby[]}
 */const {getUserByName} = require("../controllers/user.controller");
const lobbies = [];
const games = [];
const userGame = new Map();
const lobbyTimers = new Map();
const gameTimers = new Map();
const userLobby = [];

let lastFunction = 0;
// Use this hasmap to add the username into lobby but the uniqueness in the lobby is by the user id
// IT is also possible to send a object user into lobby {username: "username", id: "id"}

/**
 * The number of connected users.
 * @type {number}
 */
let usersNum = 0;

queueEmitter.on('functionProcessed', (user_id) => {
    console.log("Function processed for user: " + user_id);
    lastFunction = user_id;
});

/**
 * Finds a lobby by its price.
 * @param {number} price - The price to search for.
 * @returns {Lobby|undefined} The lobby if found, or undefined if not found.
 */
function findLobbyByPrice(price) {
    return lobbies.find((lobby) => lobby.price == price && !lobby.isLobbyFull());
}

/**
 * Finds a lobby by its ID.
 * @param {string} id - The ID to search for.
 * @returns {Lobby|undefined} The lobby if found, or undefined if not found.
 */
function findLobbyById(id) {
    return lobbies.find((lobby) => lobby.id == id);
}

/**
 * Creates a new lobby.
 * @param {number} price - The price for the lobby.
 * @param {number} maxUsers - The maximum number of users allowed in the lobby.
 * @param {number} minUsers - The minimum number of users required to start the lobby.
 * @returns {Lobby} The newly created lobby.
 */
async function createLobby(price, maxUsers, minUsers, user, socket) {
    console.log("Creating lobby");
    const lobby = new Lobby(crypto.randomBytes(8).toString("hex"), price, maxUsers, minUsers);
    userGame.set(user, lobby.id);
    addToQueue(lobby.id, async () => {
        await web3.createGame(lobby.id, price, maxUsers);
    })

    while(lastFunction != lobby.id){
        await new Promise((resolve) => setTimeout(resolve, 1000));
    }
    lobbies.push(lobby);
    return lobby;
}

/**
 * Formats a given lobby into a simplified object.
 * @param {Lobby} lobby - The lobby to format.
 * @returns {Object} A reduced representation of the lobby with essential information.
 */
function formatLobby(lobby) {
    userNames = [];
    for (let user of lobby.users) {
        userNames.push(user.username);
    }
    const reducedLobby = {
        id: lobby.id,
        price: lobby.price,
        users: userNames,
        maxUsers: lobby.maxUsers,
        minUsers: lobby.minUsers,
    };
    return reducedLobby;
}

/**
 * Formats a list of users into a simplified array.
 * @param {Object[]} userList - A list of user objects.
 * @returns {string[]} A reduced array containing usernames.
 */
function formatListOfUsers(userList){
    const reducedList = [];
    for (let user of userList) {
        reducedList.push(user.username);
    }
    return reducedList;
}

/**
 * Creates a new game instance.
 * @param {string} lobbyId - The ID of the lobby.
 * @param {Object[]} users - An array of user objects participating in the game.
 * @param {number} pot - The total pot for the game.
 * @returns {Promise<Game>} The newly created game instance.
 */
async function createGame(lobbyId, users, pot) {
    const game = new Game(lobbyId, users, pot);
    await game.setGameData();
    return game;
}

/**
 * Converts a lobby into a game.
 * @param {string} lobbyId - The ID of the lobby to convert.
 * @param {SocketIO.Socket} socket - The socket instance.
 */
async function convertLobbyToGame(lobbyId, socket) {
    let time = 30;
    const lobby = findLobbyById(lobbyId);
    const game = await createGame(
        lobbyId,
        lobby.getLobbyMembers(),
        lobby.price * lobby.users.length,
    );
    for (let user of lobby.users) {
        userGame.set(user, game.id);
    }
    await game.setGameData();
    const reducedGame = {
        id: game.id,
        category: game.category,
        pot: game.pot,
    };
    let users = []
    for (let user of lobby.users) {
        users.push(user.username);
    }
    reducedGame.users = users;
    games.push(game);
    socket.to(lobbyId).emit("startGame", reducedGame);
    socket.emit("startGame", reducedGame);
    lobbies.splice(lobbies.indexOf(lobby), 1);
    const interval = setInterval(async () => {
        time--;
        socket.to(lobbyId).emit("timer", time);
        socket.emit("timer", time);
        if (time === 0) {
            clearInterval(interval);
            showQuestions(game, socket, 0);
        }
    }, 1000);
}

/**
 * Shows a question to the players in the game.
 * @param {Game} game - The game instance.
 * @param {SocketIO.Socket} socket - The socket instance.
 * @param {number} questionNumber - The number of the current question.
 */
async function showQuestions(game, socket, questionNumber) {
    if (questionNumber === 10) return;

    const showQuestion = async () => {
        const question = game.gameData[questionNumber];
        const reducedAnswers = question.answers.map((answer) => ({
            text: answer.text,
        }));

        const reducedQuestion = {
            text: question.question.text,
            fact: question.question.fact,
            difficulty: question.question.difficulty,
            answers: reducedAnswers,
            number: questionNumber,
        };

        socket.to(game.getGameId()).emit("question", reducedQuestion);
        socket.emit("question", reducedQuestion);
    };

    const wait = async (seconds, socket, roomId, setTimer) => {
        for (let time = seconds; time > 0; time--) {
            if (setTimer) {
                gameTimers.set(roomId, time);
            }
            socket.to(roomId).emit("timer", time);
            socket.emit("timer", time);
            await new Promise((resolve) => setTimeout(resolve, 1000));
        }
        gameTimers.delete(roomId);
    };

    while (questionNumber < 10) {
        await showQuestion();
        await wait(10, socket, game.id, true);
        let kickedUsers = await game.kickUser(questionNumber);
        kickedUsers = formatListOfUsers(kickedUsers);

        socket.to(game.getGameId()).emit("kickedUsers", kickedUsers);
        socket.emit("kickedUsers", kickedUsers);
        for (let user of kickedUsers) {
            userGame.delete(user);
        }
        if (game.isFinished()) {
            games.splice(games.indexOf(game), 1);
            return;
        }

        socket.to(game.getGameId()).emit("closeQuestion");
        socket.emit("closeQuestion");

        const users = game.getActivePlayers();
        socket.to(game.getGameId()).emit("numPlayers", users);
        socket.emit("numPlayers", users);
        await wait(15, socket, game.id, false);
        questionNumber++;

        if (questionNumber === 10) {
            socket.to(game.getGameId()).emit("endGame", game.getWinnerObject());
            socket.emit("endGame", game.getWinnerObject());
            const usersAndAnswers =  game.getListOfUsersAndQuestions();
            const users = usersAndAnswers[0];
            const answers = usersAndAnswers[1];

            addToQueue("owner", async () => {
                await web3.updateParticipantOfGame(game.id, users, answers);
            });

            addToQueue("owner", async () => {
                await web3.dealMoneyToWinner(game.id);
            });
            dealRewards(game.getWinners());
            
            games.splice(games.indexOf(game), 1);
            return;
        }
    }
}

/**
 * Distributes rewards to the winners.
 * @param {string[]} winners - An array of winner usernames.
 */
async function dealRewards(winners){

    console.log("Winners: " + winners);
    for (let winner of winners) {
        console.log("winner: " +winner);
        try{
            const affiliationCode = await getAffiliationCode(winner);
            console.log("code: " + affiliationCode);
            const userId = await getUserIdByRewardCode(affiliationCode);
            console.log("user_id: " + userId);

            addToQueue("owner", async () => {
                await web3.addBalanceToUser(userId.toString(), 0.0005);
            });

        }catch (e) {
            continue;
        }

    }
}

/**
 * Initializes a timer for the lobby.
 * @param {string} lobbyId - The ID of the lobby.
 * @param {SocketIO.Socket} socket - The socket instance.
 */
function timer(lobbyId, socket) {
    let time = 30; 
    const interval = setInterval(async () => {
        time--;
        socket.to(lobbyId).emit("timer", time);
        socket.emit("timer", time);
        if (time === 0) {
            clearInterval(interval);
            convertLobbyToGame(lobbyId, socket);
        }
    }, 1000);
    lobbyTimers.set(lobbyId, interval);
}

/**
 * Exports a function to set up socket.io event handlers.
 * @param {SocketIO.Server} io - The Socket.IO server instance.
 */
module.exports = (io) => {
    io.on("connection", (socket) => {
        logger.info("Client connected");
        usersNum++;

        socket.on("createLobby", (data) => {
            if (!data.price) {
                socket.emit("error", "Not price provided");
                return;
            }
            if (!data.username) {
                socket.emit("error", "Not username provided");
                return;
            }
            if (!data.maxUsers) {
                socket.emit("error", "Not maxUsers provided");
                return;
            }
            if (!data.minUsers) {
                socket.emit("error", "Not minUsers provided");
                return;
            }
            const lobby = createLobby(data.price, data.maxUsers, data.minUsers, socket);
            lobby.addUser({username:data.username, userId: data.userId});
            socket.join(lobby.id);
            logger.info(`User ${data.username} created lobby ${lobby.id}`);
            socket.emit("lobbyCreated", lobby);
        });

        socket.on("checkUser", (data) => {
            if (!data.userId) {
                socket.emit("error", "Not username provided");
                return;
            }

            if (userGame.has(data.userId)){
                const lobbyId = userGame.get(data.userId);
                const lobby = findLobbyById(lobbyId);
                if(!lobby){
                    socket.emit("searchingUpdated", "Adding user to the Lobby!");
                    return;
                }
                if(lobby.getLobbyMembers().length > 0){
                    socket.join(lobbyId);
                    socket.emit("lobby", formatLobby(lobby));
                }
                else {
                    userGame.delete(data.userId);
                    socket.emit("searchingUpdated", "Adding user to the Lobby!");
                }

            } else {
                socket.emit("error", "lobbyNotFound");
            }
        });

        socket.on("join", async (data) => {
            if(userLobby.includes(data.userId)){
                return;
            }
            let lobby;
            if(!data.userId){
                socket.emit("error", "Not userId provided");
                return;
            }
            userLobby.push(data.userId)
            if (!data.price) {
                socket.emit("error", "Not price provided");
                return;
            }
            if (!data.username) {
                socket.emit("error", "Not username provided");
                return;
            }
            lobby = findLobbyByPrice(data.price);

            if (!lobby) {
                console.log("Creating new lobby");
                socket.emit("searchingUpdated","Creating new Lobby!");
                lobby = await createLobby(data.price, 10, 2, {username:data.username, userId: data.userId}, socket);
            }
            userGame.set(data.userId, lobby.id);
            console.log("Lobby works perfectly");
            userLobby.splice(userLobby.indexOf(data.userId), 1);

            const existingTimer = lobbyTimers.get(lobby.id);
            if (existingTimer) {
                clearInterval(existingTimer);
            }
            try{                
                web3.addParticipantToGame(lobby.id, data.userId.toString()).then((res) => {
                    socket.join(lobby.id);
                    lobby.addUser({username:data.username, userId: data.userId});
                    userGame.set(data.userId, lobby.id);
                    socket.emit("lobby", formatLobby(lobby));
                    socket.to(lobby.id).emit("lobby", formatLobby(lobby));
                    if (lobby.hasEnoughUsers()) {
                        timer(lobby.id, socket);
                    }
                }).catch((err) => {
                    console.log(err);
                    socket.emit("error", "busyBlockchain");
                });
            }catch (e) {
                socket.emit("error", "NotPossibleToJoin");
            }
            

        });

        socket.on("leave", (data) => {
            if (!data.id) {
                socket.emit("error", "Not ID provided");
                return;
            }
            if (!data.username) {
                socket.emit("error", "Not username provided");
                return;
            }
            let lobby = findLobbyById(data.id);
            if (!lobby) {
                socket.emit("error", "lobbyNotFound");
                return;
            }
            socket.leave(lobby.id);
            logger.info(`User ${data.username} left lobby ${lobby.id}`);
            lobby.removeUser(data.userId);
            if (lobby.isLobbyEmpty()) {
                lobbies.splice(lobbies.indexOf(lobby), 1);
                logger.info(`Lobby ${lobby.id} was deleted`);
            }
            lobby = findLobbyById(lobbyId);
            for (let user of lobby.users) {
                delete user.userId;
            }
            socket.emit("left", formatLobby(lobby));
            userGame.delete(data.username);
            socket.to(lobby.id).emit("lobby", formatLobby(lobby));
        });

        socket.on("disconnect", () => {
            usersNum--;
            console.log("Client disconnected");
        });
        socket.on("getUsersNum", () => {
            socket.emit("usersNum", usersNum);
        });
        socket.on("getLobbies", () => {
            socket.emit("lobbies", lobbies);
        });
        socket.on("emoji", (data) => {
            if (!data.id) {
                socket.emit("error", "Not ID provided");
                return;
            }
            if (!data.emoji) {
                socket.emit("error", "Not emoji provided");
                return;
            }
            if (!data.username) {
                socket.emit("error", "Not username provided");
                return;
            }
            logger.info(`emoji ${data.emoji} was sent to lobby ${data.id}`);
            socket.to(data.id).emit("emoji", { emoji: data.emoji, username: data.username });
            socket.emit("emoji", { emoji: data.emoji, username: data.username });
        });

        socket.on("answer", async (data)=> {
            if (!data.gameId) {
                socket.emit("error", "Not gameId provided");
                return;
            }
            if (!data.userId) {
                socket.emit("error", "Not user id provided");
                return;
            }
            if (data.questionNumber === undefined) {
                socket.emit("error", "Not questionNumber provided");
                return;
            }
            if (data.answer === undefined) {
                socket.emit("error", "Not answer provided");
                return;
            }
            
            const game = games.find((game) => game.id === data.gameId);
            if (!game) {
                socket.emit("error", "gameNotFound");
                return;
            }
            if (!gameTimers.get(game.id)) {
                socket.emit("error", "notTimer");
                return;
            }
            if (gameTimers.get(game.id) === 0) {
                socket.emit("error", "notTimer");
                return;
            }

            const isCorrect = game.addNewAnswer(data.userId, data.answer, data.questionNumber);
            if (!isCorrect) {
                socket.emit("wrongAnswer", data.username);
                socket.leave(game.id);
                userGame.delete(data.username);
                const user_id = data.userId;
                if (game.isFinished()) {
                    const usersAndAnswers =  game.getListOfUsersAndQuestions();
                    const users = usersAndAnswers[0];
                    const answers = usersAndAnswers[1];
                    games.splice(games.indexOf(game), 1);
                    addToQueue("owner", async () => {
                        await web3.updateParticipantOfGame(game.id, users, answers);
                    });
                }
            }
            socket.emit("answerResponse", isCorrect);
        });
    });
};

