/**
 * Represents a lobby where users can join and interact.
 */
class Lobby {
    /**
     * Creates a new lobby.
     * @param {string} id - The unique ID of the lobby.
     * @param {number} price - The price for joining the lobby.
     * @param {number} maxUsers - The maximum number of users allowed in the lobby.
     * @param {number} minUsers - The minimum number of users required to start the lobby.
     */
    constructor(id, price, maxUsers, minUsers) {
        this.id = id;
        this.price = price;
        this.users = [];
        this.maxUsers = maxUsers;
        this.minUsers = minUsers;
    }

    /**
     * Adds a user to the lobby.
     * @param {string} user - The username of the user to add.
     */
    addUser(user) {
        for (let i = 0; i < this.users.length; i++) {
            if (this.users[i].userId === user.userId) {
                return;
            }
        }
        this.users.push(user);
    }

    /**
     * Removes a user from the lobby.
     * @param {string} user - The username of the user to remove.
     */
    removeUser(userId) {
        for (let i = 0; i < this.users.length; i++) {
            if (this.users[i].userId === userId) {
                this.users.splice(i, 1);
                break;
            }
        }
    }

    /**
     * Gets the list of users in the lobby.
     * @returns {string[]} An array of usernames of users in the lobby.
     */
    getLobbyMembers() {
        return this.users;
    }

    /**
     * Checks if the lobby is full.
     * @returns {boolean} `true` if the lobby is full, `false` otherwise.
     */
    isLobbyFull() {
        return this.users.length === this.maxUsers;
    }

    /**
     * Checks if the lobby is empty.
     * @returns {boolean} `true` if the lobby is empty, `false` otherwise.
     */
    isLobbyEmpty() {
        return this.users.length === 0;
    }

    /**
     * Checks if the lobby has enough users to start.
     * @returns {boolean} `true` if the lobby has enough users to start, `false` otherwise.
     */
    hasEnoughUsers() {
        return this.users.length >= this.minUsers;
    }
}

// Export the Lobby class
module.exports = { Lobby };
