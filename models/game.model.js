const questionController = require("../controllers/question.controller");
const categoryController = require("../controllers/category.controller");
const answerController = require("../controllers/answer.controller");


/**
 * Represents a game session.
 */
class Game {
    /**
     * Creates a game instance.
     * 
     * @param {string} id - The unique identifier for the game.
     * @param {Array} users - Array of user objects participating in the game.
     * @param {number} pot - The total pot value for the game.
     */
    constructor(id, users, pot) {
        this.id = id;
        this.users = users;
        this.gameData = [];
        this.category = {};
        this.usersAnswers = new Map();
        this.pot = pot;
        this.usersStatus = new Map();
        for (let user of this.users) {
            this.usersStatus.set(user.userId, true);
        }
    }

    /**
     * Sets the game data including questions and answers.
     * 
     * @returns {Promise<Array>} The game data including questions and their respective answers.
     * @throws {Error} If no categories or questions are found.
     */
    async setGameData() {
        let questions;

        this.category = await categoryController.getRandomCategory();
        if (!this.category) throw new Error("No categories found");
        if (this.category.name == "Random") {
            questions = await questionController.getRandomQuestions(10);
        } else {
            questions = await questionController.getRandomQuestionsByCategory(
                this.category._id,
                10,
            );
        }
        for (let i = 0; i < questions.length; i++) {
            const answers = await answerController.getAnswerForQuestion(questions[i]._id);
            this.gameData[i] = {
                question: questions[i],
                answers: answers,
            };
        }
        for (let user of this.users) {
            this.usersAnswers.set(user.userId, []);
        }
        return this.gameData;
    }
    /**
     * Adds a new answer for a user.
     * 
     * @param {string} userId - The ID of the user.
     * @param {string} answer - The answer provided by the user.
     * @param {number} index - The index of the question being answered.
     * @returns {boolean} True if the answer is correct, false otherwise.
     */

    /**
     * Adds a new answer for a user.
     * 
     * @param {string} userId - The ID of the user.
     * @param {string} answer - The answer provided by the user.
     * @param {number} index - The index of the question being answered.
     * @returns {boolean} True if the answer is correct, false otherwise.
     */
    addNewAnswer(userId, answer, index) {
        this.usersAnswers.get(userId)[index] = answer;
        if (this.usersStatus.get(userId) === true) {
            const answerVerified = this.verifyAnswer(index, answer);
            if (answerVerified) {
                this.usersStatus.set(userId, true);
            } else {
                this.usersStatus.set(userId, false);
            }
            return answerVerified;
        }
        return false;
    }

    /**
     * Verifies the answer to a specific question.
     * 
     * @param {number} questionNumber - The index of the question.
     * @param {string} answer - The answer to verify.
     * @returns {boolean} True if the answer is correct, false otherwise.
     */
    verifyAnswer(questionNumber, answer) {
        return this.gameData[questionNumber].answers[answer].is_correct;
    }

    /**
     * Kicks a user from the game if they fail to answer.
     * 
     * @param {number} answerNumber - The answer number to check.
     * @returns {Array} Array of users who have been kicked.
     */
    kickUser(answerNumber) {
        const users = [];
        for (let user of this.users) {
            if (this.usersAnswers.get(user.userId)[answerNumber] == undefined) {
                this.usersStatus.set(user.userId, false);
                users.push(user);
            }
        }
        return users;
    }

    getGameId() {
        return this.id;
    }

    getcategory() {
        return this.category;
    }

    getPot() {
        return this.pot;
    }

    getUsersStatus() {
        return this.usersStatus;
    }

    /**
     * Determines whether the game is finished.
     * 
     * @returns {boolean} True if the game is finished, false otherwise.
     */
    isFinished() {
        for (let user of this.users) {
            if (this.usersStatus.get(user.userId) === true) {
                return false;
            }
        }
        return true;
    }

    /**
     * Calculates the final pot to be distributed among winners.
     * 
     * @param {number} numberOfWinners - The number of winners in the game.
     * @returns {number} The final pot amount for each winner.
     */
    getFinalPot(numberOfWinners) {
        return (this.pot * 0.8) / numberOfWinners;
    }

    /**
     * Retrieves the winners and the final pot amount.
     * 
     * @returns {object} An object containing the winners and the final pot amount.
     * @throws {Error} If there are no winners.
     */
    getWinnerObject() {
        const winners = [];
        for (let user of this.users) {
            if (this.usersStatus.get(user.userId) === true && this.usersAnswers.get(user.userId).length == 10) {
                winners.push(user.username);
            }
        }
        const winnerObj = {
            winners: winners,
            pot: this.getFinalPot(winners.length),
        };
        return winnerObj;
    }

    /**
     * Retrieves the usernames of a user based on its ID.
     * 
     * @param {string} userId - The ID of the user.
     * @returns {string|null} The username of the user, or null if not found.
     */
    getUsersName(userId){
        for (let user of this.users) {
            if (user.userId == userId) {
                return user.username;
            }
        }
        return null;
    }


    /**
     * Returns a list of active players' usernames.
     * 
     * @returns {Array} Array of usernames of active players.
     */
    getActivePlayers() {
        userNames = [];
        for (let user of this.users) {
            if (this.usersStatus.get(user.userId) === true) {
                userNames.push(user.username);
            }
        }
        return userNames;
    }

    /**
     * Retrieves a list of users and the number of questions they've answered.
     * 
     * @returns {Array} An array containing two arrays: one of user IDs and one of their corresponding question counts.
     */
    getListOfUsersAndQuestions() {
        const users = [];
        const questions = [];
        for (let user of this.users) {
            users.push(user.userId.toString());
            questions.push(this.usersAnswers.get(user.userId).length)
        }
        console.log([users, questions]);
        return [users, questions];
    }

    /**
     * Gets the list of winners' user IDs.
     * 
     * @returns {Array} An array of user IDs who won the game.
     */
    getWinners(){
        const winners = [];
        for (let user of this.users) {
            if (this.usersStatus.get(user.userId) === true && this.usersAnswers.get(user.userId).length == 10) {
                winners.push(user.userId);
            }
        }
        return winners;
    }
}

module.exports = { Game };
