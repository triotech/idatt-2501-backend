const { createCanvas } = require("canvas");

colors = ["#ff70A6", "#70D6FF", "#FF9770", "#FFD670", "#E9FF70", "#FFFFFF"];

width = 512;

/**
 * Generates a random color from an array of colors.
 *
 * @return {string} The randomly generated color.
 */
function getRandomColor() {
    return colors[Math.floor(Math.random() * colors.length)];
}

/**
 * Generates a random pattern and draws it on the specified canvas context.
 *
 * @param {CanvasRenderingContext2D} ctx - The canvas context on which to draw the pattern.
 */
function drawRandomPattern(ctx) {
    const patternChoices = [drawCircles, drawSquares, drawTriangles];
    const patternFunction = patternChoices[Math.floor(Math.random() * patternChoices.length)];
    patternFunction(ctx);
}

/**
 * Draws a specified number of circles on a canvas.
 *
 * @param {CanvasRenderingContext2D} ctx - The rendering context of the canvas.
 * @return {void} This function does not return a value.
 */
function drawCircles(ctx) {
    const count = 10 + Math.random() * 10;
    for (let i = 0; i < count; i++) {
        ctx.beginPath();
        const x = Math.random() * width;
        const y = Math.random() * width;
        const radius = Math.random() * 128;
        ctx.arc(x, y, radius, 0, Math.PI * 2);
        ctx.fillStyle = getRandomColor();
        ctx.fill();
    }
}

/**
 * Draws a random number of squares on the canvas.
 *
 * @param {object} ctx - The canvas context.
 * @return {undefined} This function does not return anything.
 */
function drawSquares(ctx) {
    const count = 10 + Math.random() * 10;
    for (let i = 0; i < count; i++) {
        const x = Math.random() * width;
        const y = Math.random() * width;
        const size = Math.random() * 128;
        ctx.fillStyle = getRandomColor();
        ctx.fillRect(x, y, size, size);
    }
}

/**
 * Draws a specified number of triangles on a canvas context.
 *
 * @param {CanvasRenderingContext2D} ctx - The canvas rendering context.
 */
function drawTriangles(ctx) {
    const count = 10 + Math.random() * 10;
    for (let i = 0; i < count; i++) {
        ctx.beginPath();
        ctx.moveTo(Math.random() * width, Math.random() * width);
        ctx.lineTo(Math.random() * width, Math.random() * width);
        ctx.lineTo(Math.random() * width, Math.random() * width);
        ctx.closePath();
        ctx.fillStyle = getRandomColor();
        ctx.fill();
    }
}

/**
 * Generates a patterned image.
 *
 * @return {Buffer} The generated image as a Buffer object.
 */
function generatePatternedImage() {
    const canvas = createCanvas(width, width);
    const ctx = canvas.getContext("2d");
    ctx.fillStyle = getRandomColor();
    ctx.fillRect(0, 0, width, width); // Fill the background

    drawRandomPattern(ctx);
    return canvas.toBuffer();
}

/**
 * Creates a new image file for the given user.
 *
 * @param {string} user - The user's name.
 * @return {string} The path to the newly created image file.
 */
function makeNewImage(user) {
    // Usage example:
    const fs = require("fs");
    const imageBuffer = generatePatternedImage();
    const path = `images/${user}.png`;
    fs.writeFileSync(path, imageBuffer);
    return path;
}

module.exports = {
    makeNewImage,
};
