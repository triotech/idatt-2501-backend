const { MongoClient, ServerApiVersion } = require("mongodb");
require("dotenv").config();

const uri =
    "mongodb+srv://tokentrivia_user:" +
    process.env.DATABASE_PASSWORD +
    "@triotech.kylsy6s.mongodb.net/?retryWrites=true&w=majority";

/**
 * Connects to the database server and returns the connected client.
 *
 * @return {MongoClient} The connected client.
 */
async function connectToDatabase() {
    const client = new MongoClient(uri, {
        serverApi: {
            version: ServerApiVersion.v1,
            strict: true,
            deprecationErrors: true,
        },
    });

    try {
        await client.connect();
        return client;
    } catch (error) {
        console.error("Error connecting to MongoDB:", error);
        throw error;
    }
}

module.exports = { connectToDatabase };
