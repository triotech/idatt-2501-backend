const web3 = require('./web3');
const EventEmitter = require('events');

/**
 * QueueEmitter class extending from EventEmitter to handle queue events.
 */
class QueueEmitter extends EventEmitter {}
const queueEmitter = new QueueEmitter();

let isProcessing = false;
const functionQueue = [];

/**
 * Adds a function to the queue for processing.
 * 
 * @param {string} user_id - The ID of the user for whom the function is being queued.
 * @param {Function} func - The function to be added to the queue.
 */
function addToQueue(user_id, func) {
    for (let i = 0; i < functionQueue.length; i++) {
        if (functionQueue[i].user_id === user_id && user_id != "owner") {
            return; 
        }
    }
    functionQueue.push({ user_id, func });
    processQueue();
}

/**
 * Processes the next function in the queue.
 * 
 * @returns {Promise<void>}
 */
async function processQueue() {
    if (!isProcessing && functionQueue.length > 0) {
        const nextFunction = functionQueue.shift();
        console.log("Processing queue for user: " + nextFunction.user_id);
        isProcessing = true;
        try {
            await nextFunction.func();
            queueEmitter.emit('functionProcessed', nextFunction.user_id);
        } catch (error) {
            console.error("Error processing task:", error);
        } finally {
            isProcessing = false;
            processQueue(); 
        }
    }
}

module.exports = { addToQueue, queueEmitter };
