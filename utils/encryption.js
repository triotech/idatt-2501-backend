const bcrypt = require("bcrypt");
require("dotenv").config();

/**
 * Hashes a string password.
 *
 * @param {string} password - The password to be hashed.
 * @return {Promise<string>} - A promise that resolves to the hashed password.
 */
const hashString = async (password) => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
};

/**
 * Compare two strings asynchronously.
 *
 * @param {string} password - The password to be compared.
 * @param {string} hash - The hash value to be compared against.
 * @return {Promise<boolean>} Returns a promise that resolves to a boolean indicating whether the strings match.
 */
const compareString = async (password, hash) => {
    return await bcrypt.compare(password, hash);
};

module.exports = { hashString, compareString };
