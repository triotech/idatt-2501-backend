const jwt = require("jsonwebtoken");
require("dotenv").config();

const JWT_SECRET = process.env.JWT_SECRET;

/**
 * Verifies a JSON Web Token (JWT).
 *
 * @param {string} token - The JWT to be verified.
 * @return {object} The decoded token if it is valid.
 * @throws {Error} If the token is missing or invalid.
 */
async function verifyJWT(token) {
    try {
        if (!token) {
            throw new Error("Invalid token: Token is missing.");
        }

        const decodedToken = jwt.verify(token, JWT_SECRET);
        return decodedToken;
    } catch (error) {
        throw new Error("Invalid token: " + error.message);
    }
}

/**
 * Decrypts a token and returns the decoded value.
 *
 * @param {string} token - The token to be decrypted.
 * @return {object} The decoded token.
 */
async function decryptToken(token) {
    try {
        if (!token) {
            throw new Error("Invalid token: Token is missing.");
        }
        const decodedToken = jwt.decode(token);
        return decodedToken;
    } catch (error) {
        throw new Error("Invalid token: " + error.message);
    }
}

module.exports = {
    verifyJWT,
    decryptToken,
};
