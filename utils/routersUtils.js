const express = require("express");
const router = express.Router();
const { verifyJWT } = require("./verifyJWT");
const { logger } = require("./log");
const authorityController = require("../controllers/authority.controller");

/**
 * Validates a JWT token.
 * 
 * @param {express.Response} res - The express response object.
 * @param {string} jwt - The JWT token to be validated.
 * @returns {Promise<boolean>} True if the token is invalid or not provided, otherwise false.
 */
async function validateToken(res, jwt){
    if (!jwt) {
        res.status(401).send("No JWT provided");
        logger.error("No JWT provided");
        return true;
    }
    try{
        await verifyJWT(jwt);
        return false;
    }catch(error){
        logger.error("Invalid token provided");
        res.status(401).send("Invalid token provided");
        return true;
    }
}

/**
 * Validates if the user has admin authority.
 * 
 * @param {express.Response} res - The express response object.
 * @param {string} jwt - The JWT token to check the authority for.
 * @returns {Promise<boolean>} True if the user does not have admin authority, otherwise false.
 */
async function validateAuthority(res,jwt){
    if(await authorityController.getAuthority(jwt) != "admin"){
        logger.error("Invalid authority");
        res.status(423).send("Invalid authority");
        return true;
    }

    return false;
}

/**
 * Checks if the user is a MetaMask user.
 * 
 * @param {express.Response} res - The express response object.
 * @param {string} jwt - The JWT token to verify the user type.
 * @returns {Promise<boolean>} True if the user is a MetaMask user, otherwise false.
 */
async function isMetamaskUser(res,jwt){
    try {
        const decodedToken = await verifyJWT(jwt);
        const isMetamaskUser = decodedToken.tempUser?.metamask;
        if(isMetamaskUser){
            logger.error("Invalid authority");
            res.status(423).send("Invalid authority");
            return true;
        }
        return false;
    }catch(error){
        logger.error("Invalid authority due to error: " + error.message);
        res.status(423).send("Invalid authority due to error: " + error.message);
        return true;
    }
}

module.exports = {
    validateToken,
    validateAuthority,
    isMetamaskUser,
}
