const crypto = require("crypto");

/**
 * Generates a random hash of the specified length.
 *
 * @param {number} length - The length of the hash.
 * @return {string} - The generated random hash.
 */
function generateRandomHash(length) {
    return crypto
        .randomBytes(Math.ceil(length / 2))
        .toString("hex")
        .slice(0, length);
}

/**
 * Generates a random URL by combining the current timestamp and a random hash.
 *
 * @return {string} The generated random URL.
 */
function generateRandomURL() {
    const timestamp = Date.now(); // Get the current timestamp in milliseconds
    const randomHash = generateRandomHash(8); // Generate a random 8-character hash (adjust length as needed)

    const randomURL = `${timestamp}-${randomHash}`;
    return randomURL;
}

module.exports = {
    generateRandomURL,
};
