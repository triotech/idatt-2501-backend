const { Web3 } = require("web3");;
require("dotenv").config();

const SEPOLIA_RPC_URL = process.env.SEPOLIA_RPC_URL;
const CONTRACT_ADDRESS = process.env.CONTRACT_ADDRESS;
const SEPOLIA_PRIVATE_KEY = process.env.SEPOLIA_PRIVATE_KEY;

const web3 = new Web3(new Web3.providers.HttpProvider(SEPOLIA_RPC_URL));
web3.eth.Contract.handleRevert = true;

const abi = require("../contracts/ABI/Storage.json");
const contract = new web3.eth.Contract(abi, CONTRACT_ADDRESS);
const gameIdToNumber = new Map();

/**
 * Generates a random number based on the current timestamp.
 * 
 * @returns {Promise<number>} A randomly generated number.
 */
async function generateRandomNumber() {
    const randomDecimal = Math.random();
    const timestamp = Date.now();
    const randomNumber = Math.floor(randomDecimal * timestamp);
    return randomNumber;
}

/**
 * Performs interactions with a user, such as depositing to the owner and adding the user.
 * 
 * @param {string} user_id - The ID of the user to interact with.
 * @returns {Promise<void>}
 * @throws {Error} If an error occurs during the interaction.
 */
async function interact(user_id) {
    try {
        const transactions = await contract.methods.getTransactions(user_id).call();
        if(transactions.length > 0){
            return;
        }

        await depositToOwner();
        await addUser(user_id);
        await addBalanceToUser(user_id);
        await contract.methods.getTokens("owner").call();
    } catch (err) {
        console.log("Error while registering user " + user_id);
        console.log(err);
    }
}

/**
 * Deposits a fixed amount to the owner's account.
 * 
 * @returns {Promise<void>}
 * @throws {Error} If an error occurs during the deposit.
 */
async function depositToOwner() {
    try {
        const account = await web3.eth.accounts.privateKeyToAccount(SEPOLIA_PRIVATE_KEY);
        const estimatedGas = await web3.eth.estimateGas({
            from: account.address,
            to: CONTRACT_ADDRESS,
            value: web3.utils.toWei("0.01", "ether"),
            data: contract.methods.deposit("owner").encodeABI(),
        });

        const gasPrice = await web3.eth.getGasPrice();

        const txObject = {
            from: account.address,
            to: CONTRACT_ADDRESS,
            value: web3.utils.toWei("0.01", "ether"),
            gas: estimatedGas,
            gasPrice: gasPrice,
            data: contract.methods.deposit("owner").encodeABI(),
        };

        const tx = await account.signTransaction(txObject);
        const receipt = await web3.eth.sendSignedTransaction(tx.rawTransaction);

        console.log("Transaction Hash:", receipt.transactionHash);
    } catch (err) {
        throw err;
    }
}

/**
 * Adds a user to the contract.
 * 
 * @param {string} user - The user to add.
 * @param {number} [maxRetries=5] - Maximum retry attempts.
 * @param {number} [retryInterval=1000] - Interval between retries in milliseconds.
 * @returns {Promise<Object>} Transaction receipt.
 * @throws {Error} If the maximum retries are exceeded or an error occurs.
 */
async function addUser(user, maxRetries = 5, retryInterval = 1000) {
    let retries = 0;
    while (retries < maxRetries) {
    try {
        console.log("Adding user " + user);

        const account = await web3.eth.accounts.privateKeyToAccount(SEPOLIA_PRIVATE_KEY);
        const estimatedGas = await web3.eth.estimateGas({
            from: account.address,
            to: CONTRACT_ADDRESS,
            value: "0",
            data: contract.methods.register(user).encodeABI(),
        });
        const gasPrice = await web3.eth.getGasPrice();

        const txObject = {
            from: account.address,
            to: CONTRACT_ADDRESS,
            value: "0", 
            gas: estimatedGas   ,
            gasPrice: gasPrice,
            data: contract.methods.register(user).encodeABI(),
        };

        const tx = await account.signTransaction(txObject);
        const receipt = await web3.eth.sendSignedTransaction(tx.rawTransaction);
        console.log("Transaction Hash:", receipt.transactionHash);
        return receipt; 
        } catch (err) {
            if (err.class == "TransactionBlockTimeoutError") {
                console.error("Timeout error. Retrying...");
                retries++;
                await sleep(retryInterval);
            } else {
                throw err;
            }
        } 
    }
 throw new Error(`Exceeded maximum retries (${maxRetries}) for adding user ${user}`);
}

/**
 * Adds balance to a user's account.
 * 
 * @param {string} user - The user to add balance to.
 * @param {number} [amount=0.005] - The amount to be added.
 * @param {number} [maxRetries=5] - Maximum retry attempts.
 * @param {number} [retryInterval=1000] - Interval between retries in milliseconds.
 * @returns {Promise<Object>} Transaction receipt.
 * @throws {Error} If the maximum retries are exceeded or an error occurs.
 */
async function addBalanceToUser(user,amount = 0.005, maxRetries = 5, retryInterval = 1000) {
    let retries = 0;
    while (retries < maxRetries) {
        try {
            console.log("Adding balance to user " + user);

            const account = await web3.eth.accounts.privateKeyToAccount(SEPOLIA_PRIVATE_KEY);
            const gasPrice = await web3.eth.getGasPrice();
            const estimatedGas = await web3.eth.estimateGas({
                from: account.address,
                to: CONTRACT_ADDRESS,
                value: "0", 
                data: contract.methods
                    .addBalanceToUser(user, web3.utils.toWei(amount, "ether"))
                    .encodeABI(),
            });

            const txObject = {
                from: account.address,
                to: CONTRACT_ADDRESS,
                value: "0", 
                gas: estimatedGas,
                gasPrice: gasPrice,
                data: contract.methods
                    .addBalanceToUser(user, web3.utils.toWei(amount, "ether"))
                    .encodeABI(),
            };

            const tx = await account.signTransaction(txObject);
            const receipt = await web3.eth.sendSignedTransaction(tx.rawTransaction);

            console.log("Transaction Hash:", receipt.transactionHash);
            return receipt; 
        } catch (err) {
            retries++;
            sleep(retryInterval);
        }
    }
    throw new Error(`Exceeded maximum retries (${maxRetries}) for adding balance to user ${user}`);
}

/**
 * Creates a new game with specified parameters.
 * 
 * @param {string} gameId - The ID of the game to create.
 * @param {number} buyIn - The buy-in amount for the game.
 * @param {number} maxPlayers - The maximum number of players for the game.
 * @returns {Promise<void>}
 * @throws {Error} If an error occurs during the game creation.
 */
async function createGame(gameId, buyIn, maxPlayers,  retryInterval = 1000, maxRetries = 5) {
    let retries = 0;
    while (retries < maxRetries) {
        try {
            if(gameIdToNumber.has(gameId)){
                return;
            }
            console.log("Creating game " + gameId);
            const randomNumber = await generateRandomNumber();
    
            const account = await web3.eth.accounts.privateKeyToAccount(SEPOLIA_PRIVATE_KEY);
            const gasPrice = await web3.eth.getGasPrice();
            const estimatedGas = await web3.eth.estimateGas({
                from: account.address,
                to: CONTRACT_ADDRESS,
                value: "0", 
                data: contract.methods.addGame( randomNumber, buyIn * Math.pow(10,14) ,Date.now() + 1000 * 60 * 60 * 24 * 7,maxPlayers).encodeABI(),
            });
    
            const txObject = {
                from: account.address,
                to: CONTRACT_ADDRESS,
                value: "0",
                gas: estimatedGas,
                gasPrice: gasPrice,
                data: contract.methods.addGame( randomNumber, buyIn * Math.pow(10,14) ,Date.now() + 1000 * 60 * 60 * 24 * 7,maxPlayers).encodeABI(),
            };
    
            const tx = await account.signTransaction(txObject);
            const receipt = await web3.eth.sendSignedTransaction(tx.rawTransaction);
            gameIdToNumber.set(gameId, randomNumber);
            console.log("Transaction Hash:", receipt.transactionHash);
            console.log("Transaction mined");
            return;
        } catch (err) {
            if (err.class == "TransactionBlockTimeoutError") {
                console.error("Timeout error. Retrying...");
                retries++;
                await sleep(retryInterval);
            } else {
                throw err;
            }
        }
    }
}

/**
 * Adds a participant to an existing game.
 * 
 * @param {string} gameId - The ID of the game.
 * @param {string} user - The user to add to the game.
 * @returns {Promise<Object>} The last transaction receipt.
 * @throws {Error} If an error occurs during the operation.
 */
async function addParticipantToGame(gameId, user) {
    let lastReceipt = null;
    try {
        if(!gameId){
            return;
        }
        if(!gameIdToNumber.has(gameId)){
            return;
        }
        if(!user){
            return;
        }
        
        console.log("Adding user " + user + " to game " + gameId);

        const account = await web3.eth.accounts.privateKeyToAccount(SEPOLIA_PRIVATE_KEY);
        const gasPrice = await web3.eth.getGasPrice();
        const number = gameIdToNumber.get(gameId);
        const estimatedGas = await web3.eth.estimateGas({
            from: account.address,
            to: CONTRACT_ADDRESS,
            value: "0", 
            data: contract.methods.addParticipantToGame(number, user).encodeABI(),
        });
        const txObject = {
            from: account.address,
            to: CONTRACT_ADDRESS,
            value: "0", 
            gas: estimatedGas,
            gasPrice: gasPrice,
            data: contract.methods.addParticipantToGame(number, user).encodeABI(),
        };

        const tx = await account.signTransaction(txObject);
        console.log("Sending transaction");
        const receipt = await web3.eth.sendSignedTransaction(tx.rawTransaction);
        console.log("Transaction Hash:", receipt.transactionHash);
        let signedRecipe = null;
        signedRecipe = await web3.eth.getTransactionReceipt(receipt.transactionHash);
        lastReceipt = signedRecipe;
    } catch (err) {
        throw err;
    }

    return new Promise((resolve, reject) => {
        console.log("Resolving");
        resolve(lastReceipt);
    });
}

/**
 * Distributes money to the winner(s) of a game.
 * 
 * @param {string} gameId - The ID of the game.
 * @param {number} [maxRetries=5] - Maximum retry attempts.
 * @param {number} [retryInterval=1000] - Interval between retries in milliseconds.
 * @returns {Promise<Object>} Transaction receipt.
 * @throws {Error} If the maximum retries are exceeded or an error occurs.
 */
async function dealMoneyToWinner(gameId, maxRetries = 5, retryInterval = 1000) {
    let retries = 0;

    while (retries < maxRetries) {
        try {
            console.log("Dealing money to winner for game " + gameId);

            const account = await web3.eth.accounts.privateKeyToAccount(SEPOLIA_PRIVATE_KEY);
            const gasPrice = await web3.eth.getGasPrice();
            const number = gameIdToNumber.get(gameId);
            const estimatedGas = await web3.eth.estimateGas({
                from: account.address,
                to: CONTRACT_ADDRESS,
                value: "0",
                data: contract.methods.dealMoneyToWinners(number).encodeABI(),
            });

            const txObject = {
                from: account.address,
                to: CONTRACT_ADDRESS,
                value: "0",
                gas: estimatedGas,
                gasPrice: gasPrice,
                data: contract.methods.dealMoneyToWinners(number).encodeABI(),
            };

            const tx = await account.signTransaction(txObject);
            const receipt = await web3.eth.sendSignedTransaction(tx.rawTransaction);
            gameIdToNumber.delete(gameId);

            console.log("Transaction Hash:", receipt.transactionHash);
            return receipt; 
        } catch (err) {
            console.log(err);
            retries++;
            sleep(retryInterval);
        }
    }

    throw new Error(`Exceeded maximum retries (${maxRetries}) for dealing money to winner for game ${gameId}`);
}

/**
 * Updates the participants of a game.
 * 
 * @param {string} gameId - The ID of the game.
 * @param {Array<string>} users - Array of user IDs.
 * @param {Array<number>} questionNumbers - Array of question numbers answered by users.
 * @param {number} [maxRetries=5] - Maximum retry attempts.
 * @param {number} [retryInterval=1000] - Interval between retries in milliseconds.
 * @returns {Promise<Object>} Transaction receipt.
 * @throws {Error} If the maximum retries are exceeded or an error occurs.
 */
async function updateParticipantOfGame(gameId, users, questionNumbers, maxRetries = 5, retryInterval = 1000) {
    let retries = 0;

    while (retries < maxRetries) {
        try {
            console.log("Updating participants of game " + gameId);

            const account = await web3.eth.accounts.privateKeyToAccount(SEPOLIA_PRIVATE_KEY);
            const gasPrice = await web3.eth.getGasPrice();
            const number = gameIdToNumber.get(gameId);
            const estimatedGas = await web3.eth.estimateGas({
                from: account.address,
                to: CONTRACT_ADDRESS,
                value: "0",
                data: contract.methods.updateGame(number, users, questionNumbers).encodeABI(),
            });

            const txObject = {
                from: account.address,
                to: CONTRACT_ADDRESS,
                value: "0", 
                gas: estimatedGas,
                gasPrice: gasPrice,
                data: contract.methods.updateGame(number, users, questionNumbers).encodeABI(),
            };

            const tx = await account.signTransaction(txObject);
            const receipt = await web3.eth.sendSignedTransaction(tx.rawTransaction);

            console.log("Transaction Hash:", receipt.transactionHash);
            return receipt; 
        } catch (err) {
            console.log(err);
            retries++;
            sleep(retryInterval);
        }
    }

    throw new Error(`Exceeded maximum retries (${maxRetries}) for updating participants of game ${gameId}`);
}

/**
 * Pauses the execution for a specified amount of time.
 * 
 * @param {number} ms - The amount of time to sleep in milliseconds.
 * @returns {Promise<void>}
 */
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Retrieves the score table from the contract.
 * 
 * @returns {Promise<Array>} The score table.
 * @throws {Error} If an error occurs during the retrieval.
 */
async function getScoreTable(){
    const account = await web3.eth.accounts.privateKeyToAccount(SEPOLIA_PRIVATE_KEY);
    const scoreTable = await contract.methods.getScoreTable().call({from: account.address});
    return scoreTable;
}

module.exports = {
    interact,
    createGame,
    addParticipantToGame,
    dealMoneyToWinner,
    updateParticipantOfGame,
    getScoreTable,
    addBalanceToUser
};
