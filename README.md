# TokenTrivia Backend Projects

## Table of contents

1. [Description](#description)
2. [Installation](#installation)
3. [Functionalities](#functionalities)
4. [Contributions](#contribution)

## Description
This repository hosts the Node.js server for the TokenTrivia web application. Leveraging the power of [Express](https://expressjs.com/), the project efficiently sets up HTTPS endpoints, ensuring robust and secure web communication. Additionally, it utilizes [Socket.IO](https://socket.io/) for establishing real-time, bidirectional, and event-based websocket connections. This combination offers a seamless and interactive user experience, making TokenTrivia a responsive and dynamic web application.

In addition to powering the TokenTrivia web application, this project seamlessly integrates with a smart contract that dictates the economic model and game logic of the system. This integration is facilitated through a robust middleware node, powered by [Alchemy](https://www.alchemy.com/), ensuring a secure and efficient connection between the web application and the blockchain environment. This strategic linkage enables the application to leverage blockchain technology, adding a layer of transparency and security to the gaming experience.

Special thanks to Surya Kathayat, our esteemed advisor, whose invaluable guidance and expertise significantly contributed to the development of this project. We appreciate his dedication and insights throughout this journey.

## Installation
To set up TokenTrivia backend project on your local machine, following these steps:

1. Clone the repository.

- Clone with SSH:
```bash
git clone git@gitlab.stud.idi.ntnu.no:triotech/idatt-2501-backend.git
```
- Clone with HTTPS:
```bash
git clone https://gitlab.stud.idi.ntnu.no/triotech/idatt-2501-backend.git
```

2. Install packages
```bash
npm install
```

3. Run the server
```bash
npm run start
```

## Functionalities

- **Run:** To launch the TokenTrivia system server, as previously outlined, please follow the detailed steps provided in the installation section. This will ensure a smooth setup and execution of the server, aligning with the primary objective of this repository.

-**Testing:** This project also supports comprehensive testing of its functionalities. Tests are crafted using Mocha, a simple yet powerful testing framework. To execute these tests, use the following command:
```bash
npm run test
```

-**Swagger:** This project utilizes Swagger for API documentation. Swagger provides a user-friendly interface to interact with the API, allowing for easy testing and exploration of its endpoints. To view and interact with the API documentation, navigate to the Swagger UI after starting the server. This tool greatly simplifies the process of understanding and utilizing the TokenTrivia API, making it accessible for both developers and stakeholders.

1. Run the server
```bash
npm run start
```
2. Open [Swagger](http://localhost:8080/api-docs/#/)

## Contribution

To contribute to our project, please follow these steps:

1. Before making any changes, create a new issue that describes the contribution you plan to add. This will help to ensure that your changes align with our project goals and development roadmap. 
2. Once the issue has been approved, create a new branch for the issue, using a descriptive name that summarizes the changes you plan to make. 
3. When you have completed your changes, push the branch to our Git repository. This will trigger our pipeline to check your changes for any issues or errors. 
4. If the pipeline check is successful, submit a merge request to the main branch. Please include a detailed description of your changes and any relevant information that may help us review your request. 
5. After your changes have been merged, close the issue and delete the branch from the repository. This helps to keep our repository organized and maintainable.