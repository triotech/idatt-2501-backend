const { connectToDatabase } = require("../utils/mongoDB");
const { hashString, compareString } = require("../utils/encryption");
const { ObjectId } = require("mongodb");
const { makeNewImage } = require("../utils/imageGenerator");
const { generateRandomURL } = require("../utils/urlGenerator");
const { decryptToken } = require("../utils/verifyJWT");
const { interact } = require("../utils/web3");
const fs = require("fs");
const path = require("path");
const {addToQueue} = require("../utils/web3queue");
const  {getScoreTable} = require("../utils/web3");

/**
 * Validates the user name.
 *
 * @param {string} name - The user name to be validated.
 * @throws {Error} If the user name is less than 6 characters.
 * @throws {Error} If the user name contains invalid characters.
 * @return {boolean} Returns true if the user name is valid.
 */
function validateUserName(name) {
    if (name.length < 6) {const { logger } = require("../utils/log");
        throw new Error("Username must be at least 6 characters.");
    }
    const invalidCharacterPattern = /[^a-zA-Z0-9_]/;
    if (invalidCharacterPattern.test(name)) {
        throw new Error("Username cannot contain invalid characters.");
    }
    return true;
}

/**
 * Validates the user password.
 *
 * @param {string} password - The password to be validated.
 * @throws {Error} If the password length is less than 8 characters.
 * @throws {Error} If the password does not contain at least one number and one character.
 * @return {boolean} Returns true if the password is valid.
 */
function validateUserPassword(password) {
    if (password.length < 8) {
        throw new Error("Password must be at least 8 characters.");
    }

    // Check if the password contains at least one number and one character
    const hasNumber = /[0-9]/.test(password);
    const hasCharacter = /[a-zA-Z]/.test(password);

    if (!hasNumber || !hasCharacter) {
        throw new Error("Password must contain at least one number and one character.");
    }

    return true;
}

/**
 * Validates if an email address is in a valid format.
 *
 * @param {string} email - The email address to validate.
 * @throws {Error} Invalid email address.
 * @return {boolean} Returns true if the email address is valid.
 */
function validateEmail(email) {
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailPattern.test(email)) {
        throw new Error("Invalid email address.");
    }
    return true;
}

/**
 * Creates a user in the system.
 *
 * @param {Object} user - The user object containing the user's information.
 * @param {string} user.name - The name of the user.
 * @param {string} user.email - The email of the user.
 * @param {string} user.password - The password of the user.
 * @param {string} user.country - The country of the user.
 * @return {boolean} Returns true if the user is successfully created.
 * @throws {Error} Throws an error if the user data is invalid, the email or name is already in use, or the user is not created.
 */
async function createUser(user) {
    if (!user || !user.name || !user.email || !user.password || !user.country)
        throw new Error("Invalid user data");
    let client;
    try {
        validateUserName(user.name);
        validateUserPassword(user.password);
        validateEmail(user.email);
        const client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const authorityCollection = db.collection("authorities");

        if (await userCollection.findOne({ email: user.email }))
            throw new Error("The email is already in use");

        if (await userCollection.findOne({ name: user.name }))
            throw new Error("The name is already in use");

        user.password = await hashString(user.password);
        user.timestamp = new Date();
        user.avatar_url = makeNewImage(generateRandomURL());
        user.style = "linear-gradient(45deg, #FF70A6 0%, #FFD670 100%)";

        const result = await userCollection.insertOne(user);
        if (result.insertedCount === 0) throw new Error("User not created");
        
        authorityCollection.insertOne({
            user_id: new ObjectId(result.insertedId),
            authority: "user",
        });

        const registeredUser = await userCollection.findOne({ name: user.name });
        
        addToQueue(registeredUser._id.toString(), async () => {
            await interact(registeredUser._id.toString());
        });
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
    return true;
}

/**
 * Retrieves a user from the TrioTech database based on their name.
 *
 * @param {string} name - The name of the user to retrieve.
 * @return {Promise<Object>} A promise that resolves with the user object if found, or null if not found.
 */
async function getUser(name) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const user = await userCollection.findOne({ name: name });
        if (!user) throw new Error("User not found");
        delete user.password;
        delete user._id;
        return user;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves the avatar URL of a user based on their name.
 *
 * @param {string} name - The name of the user.
 * @return {string} The URL of the user's avatar.
 */
async function getUserImage(name) {
    let client; // Declare the client variable outside the try block
    try {
        if (!name) throw new Error("Invalid user data");

        client = await connectToDatabase(); // Initialize the client
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const user = await userCollection.findOne({ name: name });
        if (!user) throw new Error("User not found");
        return user.avatar_url;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Changes the style of a user and updates it in the database.
 *
 * @param {string} style - The new style to apply to the user.
 * @param {string} token - The token of the user making the request.
 * @return {boolean} - Returns true if the style was successfully changed.
 */
async function changeStyle(style, token) {
    let client; // Declare the client variable outside the try block
    try {
        client = await connectToDatabase(); // Initialize the client
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const id = await getUserId(token);
        if (!id) throw new Error("Invalid user data");
        const user = await userCollection.findOne({ _id: new ObjectId(id) });
        if (!user) throw new Error("User not found");
        await userCollection.updateOne({ _id: new ObjectId(id) }, { $set: { style: style } });
        return true;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves the user ID from a given token.
 *
 * @param {string} token - The token to decrypt and retrieve the user ID from.
 * @throws {Error} Invalid token: Token is missing.
 * @return {string} The user ID retrieved from the token.
 */
async function getUserId(token) {
    if (!token) throw new Error("Invalid token: Token is missing.");
    const decodedToken = await decryptToken(token);
    const userId = decodedToken.user?._id ?? decodedToken._id;
    return userId;
}

/**
 * Change the password for a user.
 *
 * @param {string} oldPassword - The old password of the user.
 * @param {string} newPassword - The new password to be set for the user.
 * @param {string} token - The authentication token for the user.
 * @return {boolean} Returns true if the password was successfully changed.
 */
async function changePassword(oldPassword, newPassword, token) {
    let client;
    try {
        if (!oldPassword || !newPassword) throw new Error("Invalid user data");
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const id = await getUserId(token);
        if (!id) throw new Error("Invalid user data");
        const user = await userCollection.findOne({ _id: new ObjectId(id) });
        if (!user) throw new Error("User not found");
        const validPassword = await compareString(oldPassword, user.password);
        if (!validPassword) throw new Error("Invalid password");
        await userCollection.updateOne(
            { _id: new ObjectId(id) },
            { $set: { password: await hashString(newPassword) } },
        );
        return true;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Changes the avatar of a user.
 *
 * @param {string} token - The user token.
 * @return {boolean} Returns true if the avatar was successfully changed.
 */
async function changeAvatar(token) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const id = await getUserId(token);
        if (!id) throw new Error("Invalid user data");
        const user = await userCollection.findOne({ _id: new ObjectId(id) });
        avatar_url = user.avatar_url;
        if (avatar_url) {
            const filePath = path.join(__dirname, "../", avatar_url);
            fs.unlinkSync(filePath);
        }
        await userCollection.updateOne(
            { _id: new ObjectId(id) },
            { $set: { avatar_url: makeNewImage(generateRandomURL()) } },
        );
        return true;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves the count of active users based on their last active time.
 * 
 * @returns {Promise<number>} The count of active users.
 * @throws {Error} If there's an error during the database operation.
 */
async function getActiveUsers() {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const users = await userCollection.find({}).toArray();
        let activeUsers = 0;
        users.forEach((user) => {
            const lastActive = new Date(user.last_active);
            if (lastActive > new Date(Date.now() - 3600000)) activeUsers++;
        });
        return activeUsers;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves a list of users whose names partially match the given input.
 * 
 * @param {string} partialName - The partial name to search for.
 * @returns {Promise<Array>} An array of user objects with matching names.
 * @throws {Error} If there's an error during the database operation.
 */
async function getUsersByPartialName(partialName) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const authorityCollection = db.collection("authorities");
        const regex = new RegExp(partialName, 'i');
        const users = await userCollection.find({ name: { $regex: regex } }).limit(5).toArray();
        for (let i = 0; i < users.length; i++) {
            const authority = await authorityCollection.findOne({ user_id: new ObjectId(users[i]._id) });
            delete users[i].password;
            delete users[i]._id;
            delete users[i].style;
            if(!authority){
                users[i].authority = "user";
            }else{
                users[i].authority = authority.authority;
            }
        }
        return users;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves the user ID by their name.
 * 
 * @param {string} name - The name of the user.
 * @returns {Promise<ObjectId>} The ObjectId of the user.
 * @throws {Error} If the user is not found or there's an error during the database operation.
 */
async function getUserByName(name){
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const user = await userCollection.findOne({ name: name });
        if (!user) throw new Error("User not found");
        return user._id;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Changes the country of a user.
 * 
 * @param {string} country - The new country to be set for the user.
 * @param {string} token - The JWT token of the user.
 * @returns {Promise<boolean>} True if the operation is successful.
 * @throws {Error} If the user data is invalid, user is not found, or there's an error during the database operation.
 */
async function changeCountry(country, token) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const id = await getUserId(token);
        if (!id) throw new Error("Invalid user data");
        const user = await userCollection.findOne({ _id: new ObjectId(id) });
        if (!user) throw new Error("User not found");
        await userCollection.updateOne({ _id: new ObjectId(id) }, { $set: { country: country } });
        return true;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves a formatted score table for a specific user.
 * 
 * @param {string} token - The JWT token of the user.
 * @returns {Promise<Array>} A formatted array of score data.
 * @throws {Error} If the user data is invalid, user is not found, or there's an error during the database operation.
 */
async function getFormattedScoreTable(token){
    let client;
    try{
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const id = await getUserId(token);
        if (!id) throw new Error("Invalid user data");
        const user = await userCollection.findOne({ _id: new ObjectId(id) });
        if (!user) throw new Error("User not found");

        const scoreTable = await getScoreTable();
        const newScoreTable = [];

        for ( let i = 0; i < scoreTable.length; i++){
            try {
                new ObjectId(scoreTable[i].userName);
            } catch (error) {
                continue;
            }
            const user = await userCollection.findOne({ _id: new ObjectId(scoreTable[i].userName) });
            if(!user) continue;
            if(scoreTable[i].score == 0) continue;
            newScoreTable.push({
                name: user.name,    
                country: user.country,
                score: Number(scoreTable[i].score)
            });
        }
        
        newScoreTable.sort((a, b) => {
            return b.score - a.score;
        });

        newScoreTable.splice(10);

        return newScoreTable;
    }
    catch(error){
        throw error;
    }
    finally{
        if(client){
            await client.close();
        }
    }
}


module.exports = {
    createUser,
    getUser,
    getUserImage,
    changeStyle,
    getUserId,
    changePassword,
    changeAvatar,
    getActiveUsers,
    getUsersByPartialName,
    getUserByName,
    changeCountry,
    getFormattedScoreTable
};
