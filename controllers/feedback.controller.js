const { connectToDatabase } = require("../utils/mongoDB");
const { ObjectId } = require("mongodb");
const { getUserId } = require("./user.controller");

/**
 * Adds feedback to the database.
 *
 * @param {Object} feedback - The feedback object to be added.
 * @param {string} feedback.user - The name of the user giving the feedback.
 * @param {string} feedback.title - The title of the feedback.
 * @param {string} feedback.comment - The comment of the feedback.
 * @param {Date} [feedback.timestamp] - The timestamp of the feedback. Defaults to the current date if not provided.
 * @param {string} [feedback.error_message=""] - The error message associated with the feedback. Defaults to an empty string if not provided.
 * @throws {Error} Throws an error if the feedback data is invalid.
 * @throws {Error} Throws an error if the user is not found.
 * @throws {Error} Throws an error if the feedback is not created.
 * @return {boolean} Returns true if the feedback is successfully added.
 */
async function addFeedback(feedback, jwt) {
    let client;
    try {
        if (!feedback || !feedback.title || !feedback.comment) {
            throw new Error("Invalid feedback data");
        }
        if (!feedback.timestamp) feedback.timestamp = new Date();
        const id = await getUserId(jwt);
        if (!id) throw new Error("User not found");
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const feedbackCollection = db.collection("feedbacks");
        const userCollection = db.collection("users");
        const user = await userCollection.findOne({ _id: new ObjectId(id) });
        if (!user) throw new Error("User not found");
        feedback.user_id = user._id;
        const result = await feedbackCollection.insertOne(feedback);
        if (result.insertedCount === 0) throw new Error("Feedback not created");
        return true;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves a list of feedbacks from the TrioTech database.
 *
 * @param {number} pageNumber - The page number of the feedbacks to retrieve.
 * @param {number} itemsPerPage - The number of feedbacks to retrieve per page.
 * @throws {Error} Invalid feedback data
 * @return {Promise<Array>} A promise that resolves to an array of feedbacks.
 */
async function getFeedbacks(pageNumber, itemsPerPage) {
    let client;
    try {
        if (!pageNumber || !itemsPerPage) {
            throw new Error("Invalid feedback data");
        }

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const feedbackCollection = db.collection("feedbacks");
        const skip = (pageNumber - 1) * itemsPerPage;
        const response = await feedbackCollection.find({}).sort({timestamp: -1}).skip(skip).limit(itemsPerPage).toArray();
        const count = await feedbackCollection.countDocuments();
        return { feedbacks: response, count: count };
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves the feedback with the specified ID from the TrioTech database.
 *
 * @param {string} id - The ID of the feedback to retrieve.
 * @throws {Error} Throws an error if the feedback data is invalid.
 * @return {Promise<object>} Returns a Promise that resolves to the feedback object.
 */
async function getFeedback(id) {
    let client;
    try {
        if (!id) throw new Error("Invalid feedback data");

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const feedbackCollection = db.collection("feedbacks");
        const response = await feedbackCollection.findOne({ _id: new ObjectId(id) });
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Deletes a feedback from the database.
 *
 * @param {string} id - The ID of the feedback to delete.
 * @return {Promise} A Promise that resolves to the result of the delete operation.
 */
async function deleteFeedback(id) {
    let client;
    try {
        if (!id) throw new Error("Invalid feedback data");

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const feedbackCollection = db.collection("feedbacks");
        const response = await feedbackCollection.deleteOne({ _id: new ObjectId(id) });
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Updates the feedback with the specified ID.
 *
 * @param {string} id - The ID of the feedback to update.
 * @param {object} feedback - The updated feedback data.
 * @throws {Error} If the feedback data is invalid.
 * @return {object} The result of the update operation.
 */
async function updateFeedback(id, feedback) {
    let client;
    try {
        if (!id) throw new Error("Invalid feedback data");
        if (!feedback) throw new Error("Invalid feedback data");

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const feedbackCollection = db.collection("feedbacks");
        const response = await feedbackCollection.updateOne({ _id: new ObjectId(id) }, feedback);
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves all feedbacks by user.
 *
 * @param {string} name - The name of the user.
 * @param {number} pageNumber - The page number.
 * @param {number} itemsPerPage - The number of items per page.
 * @throws {Error} Invalid user data if the name is falsy.
 * @throws {Error} User not found if the user is not found in the database.
 * @return {Promise<Array>} A promise that resolves to an array of feedbacks.
 */
async function getAllFeedbacksByUser(name, pageNumber, itemsPerPage) {
    let client;
    try {
        if (!name) throw new Error("Invalid user data");
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const feedbackCollection = db.collection("feedbacks");
        const userCollection = db.collection("users");
        const user = await userCollection.findOne({ name: name });
        if (!user) throw new Error("User not found");
        const skip = (pageNumber - 1) * itemsPerPage;
        const response = await feedbackCollection
            .find({ user_id: user._id })
            .skip(skip)
            .limit(itemsPerPage)
            .toArray();
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

module.exports = {
    addFeedback,
    getFeedbacks,
    getFeedback,
    deleteFeedback,
    updateFeedback,
    getAllFeedbacksByUser,
};
