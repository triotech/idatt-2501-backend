const { connectToDatabase } = require("../utils/mongoDB");
const { ObjectId } = require("mongodb");

/**
 * Creates an answer in the database.
 *
 * @param {Object} answer - The answer object to be created.
 * @param {string} answer.text - The text of the answer.
 * @param {string} answer.question_id - The ID of the question the answer belongs to.
 * @param {boolean} [answer.is_correct=false] - Indicates if the answer is correct. Default is false.
 * @throws {Error} - Invalid answer data if answer, answer.text, or answer.question_id is falsy.
 * @throws {Error} - The answer is already in use if an answer with the same text already exists in the database.
 * @throws {Error} - Question not found if the question with the given ID does not exist in the database.
 * @return {Promise<Object>} - A promise that resolves to the inserted answer object.
 */
async function createAnswer(answer) {
    let client;
    try {
        if (!answer || !answer.text || !answer.question_id) throw new Error("Invalid answer data");
        if (!answer.is_correct) answer.is_correct = false;

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const answerCollection = db.collection("answers");
        const questionCollection = db.collection("questions");

        const question = await questionCollection.findOne({
            _id: new ObjectId(answer.question_id),
        });
        if (!question) throw new Error("Question not found");
        answer.question_id = new ObjectId(answer.question_id);
        const response = await answerCollection.insertOne(answer);
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves an answer from the database based on the given ID.
 *
 * @param {string} id - The ID of the answer to retrieve.
 * @return {Promise<Object>} A promise that resolves to the answer object.
 * @throws {Error} If the ID is invalid or missing.
 */
async function getAnswer(id) {
    let client;
    try {
        if (!id) throw new Error("Invalid answer data");

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const answerCollection = db.collection("answers");
        const response = await answerCollection.findOne({ _id: new ObjectId(id) });
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves answers for a given question ID.
 *
 * @param {string} question_id - The ID of the question.
 * @throws {Error} Invalid question data
 * @return {Array} An array of answers for the question.
 */
async function getAnswerForQuestion(question_id) {
    let client;
    try {
        if (!question_id) throw new Error("Invalid question data");

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const answerCollection = db.collection("answers");
        const answers = await answerCollection
            .find({ question_id: new ObjectId(question_id) })
            .toArray();
        const shuffledAnswers = shuffleArray(answers);
        let selectedCorrectAnswer = false;
        let finalAnswers = [];
        for (const answer of shuffledAnswers) {
            if (answer.is_correct && !selectedCorrectAnswer) {
                finalAnswers.push(answer);
                selectedCorrectAnswer = true;
            } else {
                finalAnswers.push(answer);
            }
            if (finalAnswers.length === 4) {
                break;
            }
        }
        return finalAnswers;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Shuffles the elements of an array randomly.
 *
 * @param {Array} array - The array to be shuffled.
 * @return {Array} - The shuffled array.
 */
function shuffleArray(array) {
    const shuffledArray = [...array];
    for (let i = shuffledArray.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
    }
    return shuffledArray;
}

module.exports = {
    createAnswer,
    getAnswer,
    getAnswerForQuestion,
};
