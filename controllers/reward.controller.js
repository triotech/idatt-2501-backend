const { connectToDatabase } = require("../utils/mongoDB");
const { ObjectId } = require('mongodb');
const userController = require("./user.controller");
const crypto = require("crypto");

/**
 * Generates a random alphanumeric code.
 * 
 * @returns {string} A 6-character alphanumeric code.
 */
function generateRandomCode() {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let code = '';
  
    for (let i = 0; i < 6; i++) {
      const randomIndex = crypto.randomInt(characters.length);
      code += characters.charAt(randomIndex);
    }
  
    return code;
  }

/**
 * Checks if a reward code is unique in the database.
 * 
 * @param {Collection} rewardCodeCollection - The MongoDB collection for reward codes.
 * @param {string} code - The reward code to check.
 * @returns {Promise<boolean>} True if the code is unique, false otherwise.
 */
async function isCodeUnique(rewardCodeCollection, code) {
    const existingCode = await rewardCodeCollection.findOne({ code });
    return !existingCode;
}

/**
 * Creates a unique reward code for a user.
 * 
 * @param {string} jwt - The JWT token of the user.
 * @returns {Promise<Object>} A promise that resolves with the created reward code.
 * @throws {Error} If user ID is invalid or user is not found.
 */
async function createRewardCode(jwt) {
    
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const rewardCodeCollection = db.collection("reward");
        const userCollection = db.collection("users");

        const userId = await userController.getUserId(jwt);
        if (!userId) throw new Error("Invalid user ID");

        const user = await userCollection.findOne({ _id: new ObjectId(userId)});
        if (!user) throw new Error("User not found");

        let generatedCode;
        let codeIsUnique = false;

        while (!codeIsUnique) {
            generatedCode = generateRandomCode();
            codeIsUnique = await isCodeUnique(rewardCodeCollection, generatedCode);
        }

        await rewardCodeCollection.updateOne(
            { user_id: new ObjectId(userId) },
            { $set: { user_id: new ObjectId(userId), code: generatedCode } },
            { upsert: true }
        );

        return { code: generatedCode };
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves the user ID associated with a given reward code.
 * 
 * @param {string} code - The reward code.
 * @returns {Promise<ObjectId>} The ObjectId of the user associated with the reward code.
 * @throws {Error} If the reward code is not found.
 */
async function getUserIdByRewardCode(code) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const rewardCodeCollection = db.collection("reward");

        const rewardCode = await rewardCodeCollection.findOne({ code });
        if (!rewardCode) throw new Error("Reward code not found");

        return rewardCode.user_id;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves a user's reward code based on their JWT token.
 * 
 * @param {string} jwt - The JWT token of the user.
 * @returns {Promise<string|null>} The reward code of the user, or null if not found.
 * @throws {Error} If the user ID is invalid.
 */
async function getRewardCode(jwt){
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const rewardCodeCollection = db.collection("reward");

        const userId = await userController.getUserId(jwt);
        if (!userId) throw new Error("Invalid user ID");

        const rewardCode = await rewardCodeCollection.findOne({ user_id: new ObjectId(userId) });
        if (!rewardCode) return null;

        return rewardCode.code;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}


/**
 * Checks if a reward code exists in the database.
 * 
 * @param {string} code - The reward code to check.
 * @returns {Promise<boolean>} True if the code exists, false otherwise.
 * @throws {Error} If there is an error during the database query.
 */
async function checkIfCodeExist(code){
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const rewardCodeCollection = db.collection("reward");

        const rewardCode = await rewardCodeCollection.findOne({ code });
        if (!rewardCode) throw new Error("Reward code not found");

        return true;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

module.exports = { 
    createRewardCode,
    getUserIdByRewardCode,
    getRewardCode,
    checkIfCodeExist };
