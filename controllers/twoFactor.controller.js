const { connectToDatabase } = require("../utils/mongoDB");
const qrcode = require("qrcode");
const { authenticator } = require("@otplib/preset-default");
const { getUserId } = require("./user.controller");
const { ObjectId } = require("mongodb");

/**
 * Generates and adds two-factor authentication for a user.
 *
 * @param {string} jwt - The JWT token of the user.
 * @returns {Promise<Object>} Object containing the OTP auth URL and QR code data.
 * @throws {Error} If the JWT token is not provided, or the user is not found.
 */
async function addTwoFactor(jwt) {
    if(!jwt) throw new Error("No token provided");
    let client; // Declare the client variable outside the try block

    try {
        const id = await getUserId(jwt);
        client = await connectToDatabase(); // Initialize the client
        const db = client.db("TrioTech");
        const twoFactorCollection = db.collection("twoFactors");
        const userCollection = db.collection("users");
        const user = await userCollection.findOne({ _id: new ObjectId(id) });
        if (!user) throw new Error("User not found");
                // Generate a new 2FA secret
        const secret = authenticator.generateSecret();

        await twoFactorCollection.findOneAndUpdate(
            { user_id: new ObjectId(id) },
            {
                $set: {
                    user_id: user._id,
                    secret: secret,
                    createdAt: new Date(),
                    enable: false,
                },
            },
            { upsert: true }
        );

        const otpauthURL = authenticator.keyuri(user.name, "TokenTrivia", secret);
        const qrCodeData = await qrcode.toDataURL(otpauthURL);

        return {
            otpauthURL,
            qrCodeData,
        };
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Checks if two-factor authentication is enabled for a user based on JWT token.
 *
 * @param {string} jwt - The JWT token of the user.
 * @returns {Promise<boolean>} True if two-factor is enabled, false otherwise.
 * @throws {Error} If there's an error during the database operation.
 */
async function hasTwoFactorEnabled(jwt) {
    let client;
    try {
        const id = await getUserId(jwt);
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const twoFactorCollection = db.collection("twoFactors");
        const twoFactor = await twoFactorCollection.findOne({ user_id: new ObjectId(id) });
        if (!twoFactor) return false;
        return twoFactor.enable;
    }
    catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Checks if two-factor authentication is enabled for a user based on their name.
 *
 * @param {string} name - The name of the user.
 * @returns {Promise<boolean>} True if two-factor is enabled, false otherwise.
 * @throws {Error} If the user is not found or there's an error during the database operation.
 */
async function hasTwoFactorEnabledByName(name) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const twoFactorCollection = db.collection("twoFactors");
        const userCollection = db.collection("users");
        let user;
        if (name.includes("@")) {
            user = await userCollection.findOne({ email: name });
        } else {
            user = await userCollection.findOne({ name: name });
        }
        if (!user) throw new Error("User not found");
        const twoFactor = await twoFactorCollection.findOne({ user_id: new ObjectId(user._id) });
        if (!twoFactor) return false;
        return twoFactor.enable;
    }
    catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Removes two-factor authentication for a user.
 *
 * @param {string} jwt - The JWT token of the user.
 * @returns {Promise<void>}
 * @throws {Error} If the user is not found, or 2FA data not deleted.
 */
async function removeTwoFactor(jwt) {
    let client;
    try {
        const id = await getUserId(jwt);
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const twoFactorCollection = db.collection("twoFactors");
        const user = await twoFactorCollection.findOne({ user_id: new ObjectId(id) });
        if (!user) throw new Error("User not found");
        const result = await twoFactorCollection.deleteOne({ user_id: new ObjectId(id) });
        if (result.deletedCount === 0) {
            throw new Error("2FA data not deleted");
        }
    }
    catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Verifies the two-factor authentication token for a user.
 *
 * @param {string} name - The name of the user.
 * @param {string} input - The two-factor authentication token to verify.
 * @returns {Promise<boolean>} True if verification is successful, false otherwise.
 * @throws {Error} If the user is not found or there's an error during the database operation.
 */
async function verifyTwoFactor(name, input) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const twoFactorCollection = db.collection("twoFactors");
        const userCollection = db.collection("users");
        let user;
        if(name.includes("@")){
            user = await userCollection.findOne({ email: name });
        }
        else{
            user = await userCollection.findOne({ name: name });
        }
        if (!user) throw new Error("User not found");
        const twoFactor = await twoFactorCollection.findOne({ user_id: user._id });
        const isValid = authenticator.verify({ token: input, secret: twoFactor.secret });
        if (isValid){
            await twoFactorCollection.findOneAndUpdate(
                { user_id: user._id },
                {
                    $set: {
                        enable: true,
                    },
                },
                { upsert: true }
            );
        }
        return isValid;
    }
    catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}



module.exports = {
    addTwoFactor,
    hasTwoFactorEnabled,
    removeTwoFactor,
    verifyTwoFactor,
    hasTwoFactorEnabledByName
};
