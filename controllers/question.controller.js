const { connectToDatabase } = require("../utils/mongoDB");
const { ObjectId } = require("mongodb");

/**
 * Creates a new question in the database.
 *
 * @param {Object} question - The question object to be created.
 * @param {string} question.text - The text of the question.
 * @param {Array} question.categories - An array of category IDs associated with the question.
 * @param {string} [question.fact] - An optional fact about the question.
 * @return {Promise} - A promise that resolves with the created question object.
 * @throws {Error} - If the question data is invalid, a category is not found, or the question is already in use.
 */
async function createQuestion(question) {
    let client;
    try {
        if (!question || !question.text) throw new Error("Invalid question data");
        question.views = 0;
        let categories_ids = [];
        for (let category of question.categories) {
            categories_ids.push(new ObjectId(category));
        }
        if (!question.fact) {
            question.fact =
                "TokenTrivia is an online trivia game that rewards using crypto. It is a fun and easy way to engage in the web3 ecosystem.";
        } else {
            question.fact = question.fact;
        }
        question.categories = categories_ids;

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const questionCollection = db.collection("questions");
        const categoryCollection = db.collection("categories");
        for (let category_id of question.categories) {
            const category = await categoryCollection.findOne({ _id: category_id });
            if (!category) throw new Error("Category not found");
        }
        if (await questionCollection.findOne({ text: question.text }))
            throw new Error("The question is already in use");

        const response = await questionCollection.insertOne(question);
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves a question from the database based on its ID.
 *
 * @param {string} id - The ID of the question to retrieve.
 * @return {Promise<Object>} - A Promise that resolves to the question object.
 */
async function getQuestion(id) {
    let client;
    try {
        if (!id) throw new Error("Invalid question data");

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const questionCollection = db.collection("questions");
        const response = await questionCollection.findOne({ _id: new ObjectId(id) });
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves a set of random questions from a specific category.
 *
 * @param {ObjectId} category_id - The ObjectId of the category.
 * @param {number} count - The number of questions to retrieve.
 * @return {Promise<Array>} A promise that resolves to an array of question objects.
 * @throws {Error} If there's a problem with the database connection or the query.
 */

async function getRandomQuestionsByCategory(category_id, count) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const questionCollection = db.collection("questions");

        const totalQuestions = count;
        const countDifficult0 = Math.floor(0.4 * totalQuestions);
        const countDifficult1 = Math.floor(0.3 * totalQuestions);
        const countDifficult2 = totalQuestions - countDifficult0 - countDifficult1;

        const pipeline = [
            {
                $unwind: "$categories",
            },
            {
                $match: {
                    $and: [{ categories: new ObjectId(category_id) }, { difficulty: 0 }],
                },
            },
            {
                $sort: { views: 1 },
            },
            {
                $sample: { size: countDifficult0 },
            },
            {
                $unionWith: {
                    coll: "questions",
                    pipeline: [
                        {
                            $unwind: "$categories",
                        },
                        {
                            $match: {
                                $and: [{ categories: new ObjectId(category_id) }, { difficulty: 1 }],
                            },
                        },
                        {
                            $sample: { size: countDifficult1 },
                        },
                    ],
                },
            },
            {
                $unionWith: {
                    coll: "questions",
                    pipeline: [
                        {
                            $unwind: "$categories",
                        },
                        {
                            $match: {
                                $and: [{ categories: new ObjectId(category_id) }, { difficulty: 2 }],
                            },
                        },
                        {
                            $sample: { size: countDifficult2 },
                        },
                    ],
                },
            },
        ];

        const response = await questionCollection.aggregate(pipeline).toArray();
        response.forEach(async (question) => {
            await questionCollection.updateOne({ _id: question._id }, { $inc: { views: 1 } });
        });
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves a set of random questions, distributed across different difficulty levels.
 *
 * @param {number} count - The total number of random questions to retrieve.
 * @return {Promise<Array>} A promise that resolves to an array of question objects.
 * @throws {Error} If there's a problem with the database connection or the query.
 */
async function getRandomQuestions(count) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const questionCollection = db.collection("questions");

        const totalQuestions = count;
        const countDifficult0 = Math.floor(0.4 * totalQuestions);
        const countDifficult1 = Math.floor(0.3 * totalQuestions);
        const countDifficult2 = totalQuestions - countDifficult0 - countDifficult1;

        const pipeline = [
            {
                $match: {
                    $and: [{ difficulty: 0 }],
                },
            },
            {
                $sort: { views: 1 },
            },
            {
                $sample: { size: countDifficult0 },
            },
            {
                $unionWith: {
                    coll: "questions",
                    pipeline: [
                        {
                            $match: {
                                $and: [{ difficulty: 1 }],
                            },
                        },
                        {
                            $sample: { size: countDifficult1 },
                        },
                    ],
                },
            },
            {
                $unionWith: {
                    coll: "questions",
                    pipeline: [
                        {
                            $match: {
                                $and: [{ difficulty: 2 }],
                            },
                        },
                        {
                            $sample: { size: countDifficult2 },
                        },
                    ],
                },
            },
        ];

        const response = await questionCollection.aggregate(pipeline).toArray();
        response.forEach(async (question) => {
            await questionCollection.updateOne({ _id: question._id }, { $inc: { views: 1 } });
        });
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves a question from the database based on its text.
 *
 * @param {string} text - The text of the question.
 * @return {Promise<Object>} A promise that resolves to the question object, if found.
 * @throws {Error} If there's a problem with the database connection or the query, or if the question data is invalid.
 */
async function getQuestionByText(text) {
    let client;
    try {
        if (!text) throw new Error("Invalid question data");

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const questionCollection = db.collection("questions");
        const response = await questionCollection.findOne({ text: text });
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

module.exports = {
    createQuestion,
    getQuestion,
    getRandomQuestionsByCategory,
    getRandomQuestions,
    getQuestionByText,
};
