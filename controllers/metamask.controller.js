const ethers = require('ethers');
const e = require('express');

const jwt = require("jsonwebtoken");
require("dotenv").config();

const JWT_SECRET = process.env.JWT_SECRET;

/**
 * Extracts the Ethereum address from a given signature.
 * 
 * @param {string} originMessage - The original message that was signed.
 * @param {string} signature - The signature to verify.
 * @returns {Promise<string>} The Ethereum address recovered from the signature.
 */
async function extractAddressFromSignature(originMessage, signature) {
    return ethers.verifyMessage(originMessage, signature);
}

/**
 * Extracts the user ID from a JWT token.
 * 
 * @param {string} token - The JWT token to decode.
 * @returns {Promise<string>} The user ID extracted from the JWT token.
 * @throws {Error} Throws an error if no JWT token is provided.
 */
async function extractIdFromJWT(token) {
    if (!token) {
        throw new Error("No JWT provided");
    }
    const decodedToken = jwt.verify(token, JWT_SECRET);
    return decodedToken.tempUser._id;
}

/**
 * Performs a one-click login using MetaMask.
 * 
 * @param {Object} requestBody - The request body containing the MetaMask signature and other details.
 * @returns {Promise<Object>} An object containing the JWT token and temporary user information.
 * @throws {Error} Throws an error if request data is invalid or if the signature is invalid.
 */
async function metamaskOneClickLogin(requestBody) {
    if (!requestBody.signature || !requestBody.fingerprint || !requestBody.timestamp, !requestBody.username) {
        throw new Error("The request data was invalid");
    }
    const originMessage =`'Please sign this as a unique message: ${requestBody.fingerprint}${requestBody.timestamp}'`;

    try {
        let recoveredAddress = await extractAddressFromSignature(originMessage, requestBody.signature);
   
        if (!recoveredAddress) {
            throw new Error("Could not recover address from signature");
        }

        // Randomly generates user attributes

        const tempUser = {
            "_id": recoveredAddress,
            "name": requestBody.username,
            "email": "mm@user.com",
            "country": "ETH",
            "timestamp": requestBody.timestamp,
            "avatar_url": "/images/MetaMask_fox.svg",
            "last_active": requestBody.timestamp,
            "style": "linear-gradient(60deg, #70FFA6 0%, #70D6FF 100%)",
            "metamask": true,  
        }

        // The token for the metamask users is valid for 1 day
        return {
            jwt_token: jwt.sign({ tempUser }, JWT_SECRET, { expiresIn: "24h" }),
            user: tempUser,
        };
    } catch (error) {
        throw new Error("Invalid signature");
    }


}

module.exports = {
    metamaskOneClickLogin,
    extractIdFromJWT,
};
