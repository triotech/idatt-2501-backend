const { connectToDatabase } = require("../utils/mongoDB");
const { ObjectId } = require("mongodb");

/**
 * Creates a category in the TrioTech database.
 *
 * @param {Object} category - The category object to be created.
 * @param {string} category.name - The name of the category.
 * @param {string} category.description - The description of the category.
 * @throws {Error} Throws an error if the category data is invalid or if the category already exists.
 * @returns {boolean} Returns true if the category is successfully created.
 */
async function createCategory(category) {
    let client;
    try {
        if (!category || !category.name || !category.description)
            throw new Error("Invalid category data");

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const categoryCollection = db.collection("categories");

        if (await categoryCollection.findOne({ name: category.name }))
            throw new Error("The category is already in use");

        const result = await categoryCollection.insertOne(category);
        if (result.insertedCount === 0) throw new Error("Category not created");
        return true;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves a category from the TrioTech database based on the given ID.
 *
 * @param {string} id - The ID of the category to retrieve.
 * @return {Promise<object>} - A promise that resolves to the category object.
 */
async function getCategory(id) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const categoryCollection = db.collection("categories");
        const response = await categoryCollection.findOne({ _id: new ObjectId(id) });
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves all categories from the TrioTech database.
 *
 * @return {Promise<Array<Object>>} An array of category objects.
 */
async function getAllCategories() {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const categoryCollection = db.collection("categories");
        const response = await categoryCollection.find({}).toArray();
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}


/**
 * Retrieves a random category from the database.
 * 
 * @returns {Promise<Object>} A random category object from the database.
 * @throws {Error} Throws an error if there is a database connection or query issue.
 */
async function getRandomCategory() {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const categoryCollection = db.collection("categories");
        const response = await categoryCollection.aggregate([{ $sample: { size: 1 } }]).toArray();
        return response[0];
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves the ID of a category by its name.
 * 
 * @param {string} name - The name of the category.
 * @returns {Promise<ObjectId>} The ObjectId of the category.
 * @throws {Error} Throws an error if there is a database connection or query issue.
 */
async function getCatgoryIdByName(name) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const categoryCollection = db.collection("categories");
        const response = await categoryCollection.findOne({ name: name });
        return response._id;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

module.exports = { 
    createCategory, 
    getCategory, 
    getAllCategories, 
    getRandomCategory, 
    getCatgoryIdByName};
