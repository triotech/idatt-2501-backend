const { connectToDatabase } = require("../utils/mongoDB");
const { ObjectId } = require("mongodb");
const createHash = require("hash-generator");
const { compareString } = require("../utils/encryption");
const jwt = require("jsonwebtoken");
const { getUserId } = require("./user.controller");
require("dotenv").config();

const JWT_SECRET = process.env.JWT_SECRET;

/**
 * Adds a refresh token to the database.
 *
 * @param {object} refreshToken - The refresh token data.
 * @param {string} refreshToken.user - The user ID associated with the refresh token.
 * @param {string} refreshToken.fingerprint - The fingerprint of the device.
 * @return {Promise<object>} - A promise that resolves to the result of the database update operation.
 * @throws {Error} - If the refresh token data is invalid or the user is not found.
 */
async function addRefreshToken(refreshToken) {
    let client;
    try {
        if (!refreshToken || !refreshToken.user || !refreshToken.fingerprint)
            throw new Error("Invalid refresh token data");

        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const refreshTokenCollection = db.collection("refresh_tokens");
        const user = await userCollection.findOne({ _id: new ObjectId(refreshToken.user) });

        if (!user) throw new Error("User not found");

        refreshToken.token = createHash(32);
        refreshToken.timestamp = new Date();

        const filter = { user_id: new ObjectId(refreshToken.user) };
        const update = { $set: refreshToken };
        const options = { upsert: true }; 

        refreshToken.user_id = new ObjectId(refreshToken.user);
        delete refreshToken.user;
        const response = await refreshTokenCollection.updateOne(filter, update, options);
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Removes a refresh token from the "refresh_tokens" collection.
 *
 * @param {string} id - The ID of the refresh token to remove.
 * @return {Promise} - A promise that resolves when the refresh token is successfully removed.
 */
async function removeRefreshToken(id) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const refreshTokenCollection = db.collection("refresh_tokens");
        const response = refreshTokenCollection.deleteOne({ _id: new ObjectId(id) });
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Authenticates a user based on the provided authentication data.
 *
 * @param {Object} authentication - The authentication data.
 * @param {string} authentication.name - The username or email of the user.
 * @param {string} authentication.password - The password of the user.
 * @param {string} authentication.fingerprint - The fingerprint of the device used for authentication.
 * @return {Object} An object containing the JWT token and the user information.
 * @throws {Error} If the authentication data is invalid, user is not found, or the password is invalid.
 */
async function authenticateUser(authentication) {
    let client;
    try {
        if (!authentication || !authentication.name || !authentication.password)
            throw new Error("Invalid authentication data");
        if (!authentication.fingerprint) throw new Error("Invalid refresh token data");
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const userCollection = db.collection("users");
        const refreshTokenCollection = db.collection("refresh_tokens");
        let user;
        let newUser;
        if (authentication.name.includes("@")) {
            user = await userCollection.findOne({ email: authentication.name });
            newUser = await userCollection.findOne({ email: authentication.name });
        } else {
            user = await userCollection.findOne({ name: authentication.name });
            newUser = await userCollection.findOne({ name: authentication.name });
        }
        if (!user) throw new Error("User not found");
        if (!(await compareString(authentication.password, user.password)))
            throw new Error("Invalid password");
    
        const refreshToken = {
            user: user._id,
            fingerprint: authentication.fingerprint,
            token: createHash(32),
            timestamp: new Date(),
        };

        const filter = { user_id: new ObjectId(refreshToken.user) };
        const update = { $set: refreshToken };
        const options = { upsert: true };
        refreshToken.user_id = new ObjectId(refreshToken.user);
        delete refreshToken.user;
        await refreshTokenCollection.updateOne(filter, update, options);

        delete newUser._id;
        delete newUser.password;

        await userCollection.updateOne(
            { _id: new ObjectId(user._id) },
            { $set: { last_active: new Date() } },
        );
        return {
            jwt_token: jwt.sign({ user }, JWT_SECRET, { expiresIn: "1h" }),
            user: newUser,
        };
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves the refresh token for a given user ID.
 *
 * @param {string} user_id - The ID of the user.
 * @return {Promise<object>} A Promise that resolves to the refresh token object or null if not found.
 */
async function getRefreshToken(user_id) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const refreshTokenCollection = db.collection("refresh_tokens");
        const response = await refreshTokenCollection.findOne({ user_id: new ObjectId(user_id) });
        return response;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Generates a JSON Web Token (JWT) using the provided fingerprint and token header.
 *
 * @param {string} fingerprint - The fingerprint of the user.
 * @param {string} token_header - The token header.
 * @return {Promise<string>} Returns a Promise that resolves to the generated JWT.
 */
async function generateJWT(fingerprint, token_header) {
    let client;
    try {
        if (!fingerprint) throw new Error("Invalid user data");

        client = await connectToDatabase();
        const db = await client.db("TrioTech");
        const user_id = await getUserId(token_header);
        const refreshTokenCollection = await db.collection("refresh_tokens");

        const response = await refreshTokenCollection.findOne({
            user_id: new ObjectId(user_id),
            fingerprint: fingerprint,
        });
        if (!response) throw new Error("Invalid refresh token");

        const userCollection = await db.collection("users");
        const user = await userCollection.findOne({ _id: new ObjectId(user_id) });
        await userCollection.updateOne(
            { _id: new ObjectId(user._id) },
            { $set: { last_active: new Date() } },
        );
        user.last_active = new Date();
        const generateJWT = await jwt.sign(user, JWT_SECRET, { expiresIn: "1h" }); 
        return generateJWT;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

module.exports = {
    addRefreshToken,
    removeRefreshToken,
    authenticateUser,
    generateJWT,
};
