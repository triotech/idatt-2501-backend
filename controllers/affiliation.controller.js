const { connectToDatabase } = require("../utils/mongoDB");
const { ObjectId } = require("mongodb");
const { getUserId } = require("./user.controller");
const { getRewardCode } = require("./reward.controller");

/**
 * Adds an affiliation code for a user.
 * 
 * @param {string} jwt - The JWT token of the user.
 * @param {string} code - The affiliation code to be added.
 * @throws Will throw an error if the user tries to use their own reward code, 
 *         if the user ID is invalid, or if there is a database error.
 */
async function addAffiliationCode(jwt, code) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const affiliationCodeCollection = db.collection("affiliationCodes");
        const ownRewardCode = await getRewardCode(jwt);
        if(ownRewardCode === code) throw new Error("You can't use your own reward code");
        const userId = await getUserId(jwt);
        if (!userId) throw new Error("Invalid user ID");
        await affiliationCodeCollection.updateOne({ user_id: new ObjectId(userId) }, { $set: { code } }, { upsert: true });

    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves the affiliation code associated with a given user ID.
 * 
 * @param {string} userId - The ID of the user.
 * @returns {Promise<string|null>} The affiliation code if found, otherwise null.
 * @throws Throws an error if the affiliation code is not found or there's a database error.
 */
async function getAffiliationCode(userId) {
    let client;
    try {
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const affiliationCodeCollection = db.collection("affiliationCodes");
        try {
            new ObjectId(userId);
        } catch (error) {
            return null;
        }

        const affiliationCode = await affiliationCodeCollection.findOne({ user_id: new ObjectId(userId) });
        if (!affiliationCode) throw new Error("Affiliation code not found");

        return affiliationCode.code;
    } catch (error) {
        throw error;
    } finally {
        if (client) {
            await client.close();
        }
    }
}

/**
 * Retrieves the affiliation code for a user based on their JWT token.
 * 
 * @param {string} jwt - The JWT token of the user.
 * @returns {Promise<string>} The affiliation code associated with the user.
 * @throws Throws an error if the user ID is invalid or if there's a database error.
 */
async function getAffiliationCodeByJwt(jwt) {
    const userId = getUserId(jwt);
    if (!userId) throw new Error("Invalid user ID");

    return await getAffiliationCode(userId);
}

module.exports = {
    addAffiliationCode,
    getAffiliationCode,
    getAffiliationCodeByJwt,
};