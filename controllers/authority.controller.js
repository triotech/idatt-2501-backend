const { connectToDatabase } = require("../utils/mongoDB");
const { ObjectId } = require("mongodb");
const { getUserId } = require("./user.controller")

/**
 * Retrieves the authority level of a user based on their JWT token.
 * 
 * @param {string} jwt - The JWT token of the user.
 * @returns {Promise<string>} The authority level of the user.
 * @throws Will throw an error if no JWT is provided, no user ID is found, 
 *         no authority is found, or if there's a database error.
 */
async function getAuthority(jwt){
    let client;
    try{
        if(!jwt){
            throw new Error("No JWT provided");
        }
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const authorityCollection = db.collection("authorities");
        const user_id = await getUserId(jwt);
        if(!user_id){
            throw new Error("No user_id found");
        }
        const response = await authorityCollection.findOne({user_id: new ObjectId(user_id)});
        if(!response){
            throw new Error("No authority found");
        }
        return response.authority;
    }catch(error){
        throw error;
    }finally{
        if(client){
            await client.close();
        }
    }
}

/**
 * Changes the authority level of a user.
 * 
 * @param {string} name - The name of the user whose authority is to be changed.
 * @param {string} authority - The new authority level to be set for the user.
 * @returns {Promise<Object>} The result of the update operation.
 * @throws Will throw an error if no name or authority is provided, 
 *         if no user is found, or if there's a database error.
 */
async function changeAuthority(name, authority){
    let client;
    try{
        if(!name){
            throw new Error("No name provided");
        }
        if(!authority){
            throw new Error("No authority provided");
        }
        client = await connectToDatabase();
        const db = client.db("TrioTech");
        const authorityCollection = db.collection("authorities");
        const userCollection = db.collection("users");
        const user = await userCollection.findOne({name: name});
        if(!user){
            throw new Error("No user found");
        }
        const response = await authorityCollection.updateOne({user_id: new ObjectId(user._id)}, {$set: {authority: authority}});
        return response;
    }catch(error){
        throw error;
    }finally{
        if(client){
            await client.close();
        }
    }
}

module.exports = {
    getAuthority,
    changeAuthority
}

    