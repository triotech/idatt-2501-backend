const { expect } = require('chai');
const { Lobby } = require('../models/lobby.model');

describe('Lobby Class', () => {
    let lobby;

    beforeEach(() => {
        // Initialize a new lobby before each test
        lobby = new Lobby('123', 10, 5, 2);
    });

    it('should add a user to the lobby', () => {
        const user = { userId: 'user1', username: 'JohnDoe' };
        lobby.addUser(user);
        expect(lobby.getLobbyMembers()).to.deep.include(user);
    });

    it('should not add a duplicate user to the lobby', () => {
        const user = { userId: 'user1', username: 'JohnDoe' };
        lobby.addUser(user);
        lobby.addUser(user);
        expect(lobby.getLobbyMembers()).to.have.lengthOf(1);
    });

    it('should remove a user from the lobby', () => {
        const user = { userId: 'user1', username: 'JohnDoe' };
        lobby.addUser(user);
        lobby.removeUser('user1');
        expect(lobby.getLobbyMembers()).to.be.an('array').that.is.empty;
    });

    it('should check if the lobby is full', () => {
        for (let i = 0; i < 5; i++) {
            const user = { userId: `user${i}`, username: `User${i}` };
            lobby.addUser(user);
        }
        expect(lobby.isLobbyFull()).to.be.true;
    });

    it('should check if the lobby is empty', () => {
        expect(lobby.isLobbyEmpty()).to.be.true;
    });

    it('should check if the lobby has enough users to start', () => {
        for (let i = 0; i < 2; i++) {
            const user = { userId: `user${i}`, username: `User${i}` };
            lobby.addUser(user);
        }
        expect(lobby.hasEnoughUsers()).to.be.true;
    });
});
