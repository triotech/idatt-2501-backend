const { expect } = require("chai");
const sinon = require("sinon");
const { ObjectId } = require("mongodb");
const JWT = require("jsonwebtoken");
const mockController = (() => {
    let mockDb = {
        userCollection: null,
        refreshTokenCollection: null,
    };

    const setMockDb = (db) => {
        mockDb.userCollection = db.userCollection;
        mockDb.refreshTokenCollection = db.refreshTokenCollection;
    };

    const addRefreshToken = async (refreshToken) => {
        if (!refreshToken || !refreshToken.user || !refreshToken.fingerprint) {
            throw new Error("Invalid refresh token data");
        }

        const user = await mockDb.userCollection.findOne({ _id: new ObjectId(refreshToken.user) });
        if (!user) {
            throw new Error("User not found");
        }

        refreshToken.token = "mockedToken";
        refreshToken.timestamp = new Date();

        const filter = { user_id: new ObjectId(refreshToken.user) };
        const update = { $set: refreshToken };
        const options = { upsert: true };

        refreshToken.user_id = new ObjectId(refreshToken.user);
        delete refreshToken.user;

        const response = await mockDb.refreshTokenCollection.updateOne(filter, update, options);
        return response;
    };

    const removeRefreshToken = async (id) => {
        const response = await mockDb.refreshTokenCollection.deleteOne({ _id: new ObjectId(id) });
        return response;
    };

    const authenticateUser = async (authentication) => {
        if (!authentication || !authentication.name || !authentication.password) {
            throw new Error("Invalid authentication data");
        }

        const user = await mockDb.userCollection.findOne({ name: authentication.name });

        if (!user) {
            throw new Error("User not found");
        }

        const validPassword = true; // Mocked for testing purposes

        if (!validPassword) {
            throw new Error("Invalid password");
        }

        const refreshToken = {
            user: user._id,
            fingerprint: authentication.fingerprint,
            token: "mockedToken",
            timestamp: new Date(),
        };

        const filter = { user_id: new ObjectId(refreshToken.user) };
        const update = { $set: refreshToken };
        const options = { upsert: true };

        refreshToken.user_id = new ObjectId(refreshToken.user);
        delete refreshToken.user;

        await mockDb.refreshTokenCollection.updateOne(filter, update, options);

        delete user._id;

        return {
            jwt_token: JWT.sign({ data: "MockJWT" }, "secret", { expiresIn: "1h" }),
            user: user,
        };
    };

    const getRefreshToken = async (user_id) => {
        const response = await mockDb.refreshTokenCollection.findOne({
            user_id: new ObjectId(user_id),
        });
        return response;
    };

    const generateJWT = async (fingerprint, token_header) => {
        if (!fingerprint) {
            throw new Error("Invalid user data");
        }

        const user_id = "mockedUserId";
        const response = await mockDb.refreshTokenCollection.findOne({
            user_id: new ObjectId(user_id),
            fingerprint: fingerprint,
        });

        if (!response) {
            throw new Error("Invalid refresh token");
        }

        return {
            jwt_token: JWT.sign({ data: "MockJWT" }, "secret", { expiresIn: "1h" }),
            user: user,
        }; // Mocked JWT for testing purposes
    };

    return {
        setMockDb,
        addRefreshToken,
        removeRefreshToken,
        authenticateUser,
        getRefreshToken,
        generateJWT,
    };
})();

describe("Authentication Controller", () => {
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should add a refresh token", async () => {
        const mockRefreshToken = {
            user: "123456789012345678901234",
            fingerprint: "mockedFingerprint",
        };

        const mockUser = {
            _id: new ObjectId(mockRefreshToken.user),
        };

        const mockDb = {
            userCollection: {
                findOne: sandbox.stub().resolves(mockUser),
            },
            refreshTokenCollection: {
                updateOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);

        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves({
            userCollection: {
                findOne: sandbox.stub().resolves(mockUser),
            },
            refreshTokenCollection: {
                updateOne: sandbox.stub().resolves({}),
            },
        });

        const result = await mockController.addRefreshToken(mockRefreshToken);

        expect(result).to.not.be.undefined;
    });

    it("should remove a refresh token", async () => {
        const mockTokenId = "123456789012345678901234"; // A valid ObjectId string

        const mockDb = {
            refreshTokenCollection: {
                deleteOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.removeRefreshToken(mockTokenId);

        expect(result).to.not.be.undefined;
        expect(
            mockDb.refreshTokenCollection.deleteOne.calledOnceWithExactly({
                _id: new ObjectId(mockTokenId),
            }),
        ).to.be.true;
    });

    it("should authenticate a user", async () => {
        const mockAuthentication = {
            name: "mockedUser",
            password: "mockedPassword",
            fingerprint: "mockedFingerprint",
        };

        const mockUser = {
            _id: new ObjectId("123456789012345678901234"), // A valid ObjectId string
            name: mockAuthentication.name,
        };

        const mockDb = {
            userCollection: {
                findOne: sandbox.stub().resolves(mockUser),
            },
            refreshTokenCollection: {
                updateOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);
        sandbox.stub(require("../utils/encryption"), "compareString").resolves(true);

        const result = await mockController.authenticateUser(mockAuthentication);

        expect(result).to.not.be.undefined;
        expect(
            mockDb.userCollection.findOne.calledOnceWithExactly({
                name: mockAuthentication.name,
            }),
        ).to.be.true;
    });

    it("should get a refresh token for a user", async () => {
        const mockUserId = "123456789012345678901234"; // A valid ObjectId string

        const mockDb = {
            refreshTokenCollection: {
                findOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getRefreshToken(mockUserId);

        expect(result).to.not.be.undefined;
        expect(
            mockDb.refreshTokenCollection.findOne.calledOnceWithExactly({
                user_id: new ObjectId(mockUserId),
            }),
        ).to.be.true;
    });
});
