const { expect } = require("chai");
const sinon = require("sinon");
const { ObjectId } = require("mongodb");

const mockController = (() => {
    let mockDb = {
        feedbackCollection: null,
        userCollection: null,
    };

    const setMockDb = (db) => {
        mockDb.feedbackCollection = db.feedbackCollection;
        mockDb.userCollection = db.userCollection;
    };

    const addFeedback = async (feedback) => {
        if (!feedback || !feedback.user || !feedback.title || !feedback.comment) {
            throw new Error("Invalid feedback data");
        }
        if (!feedback.timestamp) feedback.timestamp = new Date();
        if (!feedback.error_message) feedback.error_message = "";

        const user = await mockDb.userCollection.findOne({ name: feedback.user });
        if (!user) throw new Error("User not found");
        feedback.user_id = user._id;
        delete feedback.user;
        const result = await mockDb.feedbackCollection.insertOne(feedback);
        if (result.insertedCount === 0) throw new Error("Feedback not created");
        return true;
    };

    const getFeedbacks = async (pageNumber, itemsPerPage) => {
        const skip = (pageNumber - 1) * itemsPerPage;
        const response = await mockDb.feedbackCollection
            .find({})
            .skip(skip)
            .limit(itemsPerPage)
            .toArray();
        return response;
    };

    const getFeedback = async (id) => {
        const response = await mockDb.feedbackCollection.findOne({ _id: new ObjectId(id) });
        return response;
    };

    const deleteFeedback = async (id) => {
        const response = await mockDb.feedbackCollection.deleteOne({ _id: new ObjectId(id) });
        return response;
    };

    const updateFeedback = async (id, updatedFeedback) => {
        const response = await mockDb.feedbackCollection.updateOne(
            { _id: new ObjectId(id) },
            updatedFeedback,
        );
        return response;
    };

    const getAllFeedbacksByUser = async (name, pageNumber, itemsPerPage) => {
        const user = await mockDb.userCollection.findOne({ name: name });
        if (!user) throw new Error("User not found");
        const skip = (pageNumber - 1) * itemsPerPage;
        const response = await mockDb.feedbackCollection
            .find({ user_id: user._id })
            .skip(skip)
            .limit(itemsPerPage)
            .toArray();
        return response;
    };

    return {
        setMockDb,
        addFeedback,
        getFeedbacks,
        getFeedback,
        deleteFeedback,
        updateFeedback,
        getAllFeedbacksByUser,
    };
})();

describe("Feedback Controller", () => {
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should add feedback", async () => {
        const mockFeedback = {
            user: "TestUser",
            title: "Test Title",
            comment: "Test Comment",
        };

        const mockUser = {
            _id: new ObjectId("123456789012345678901234"),
        };

        const mockDb = {
            feedbackCollection: {
                findOne: sandbox.stub(),
                insertOne: sandbox.stub().resolves({ insertedCount: 1 }),
            },
            userCollection: {
                findOne: sandbox.stub().resolves(mockUser),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.addFeedback(mockFeedback);

        expect(result);
    });

    it("should get feedbacks", async () => {
        const pageNumber = 1;
        const itemsPerPage = 10;

        const mockDb = {
            feedbackCollection: {
                find: sandbox.stub().returns({
                    skip: sandbox.stub().returns({
                        limit: sandbox.stub().returns({
                            toArray: sandbox
                                .stub()
                                .resolves([{ title: "Feedback1" }, { title: "Feedback2" }]),
                        }),
                    }),
                }),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getFeedbacks(pageNumber, itemsPerPage);

        expect(result).to.deep.equal([{ title: "Feedback1" }, { title: "Feedback2" }]);
        expect(mockDb.feedbackCollection.find.calledOnceWithExactly({})).to.be.true;
        expect(
            mockDb.feedbackCollection
                .find()
                .skip.calledOnceWithExactly((pageNumber - 1) * itemsPerPage),
        ).to.be.true;
        expect(mockDb.feedbackCollection.find().skip().limit.calledOnceWithExactly(itemsPerPage)).to
            .be.true;
    });

    it("should get a feedback by ID", async () => {
        const mockId = "123456789012345678901234"; // A valid ObjectId string

        const mockDb = {
            feedbackCollection: {
                findOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getFeedback(mockId);

        expect(result).to.deep.equal({});
        expect(mockDb.feedbackCollection.findOne.calledOnceWithExactly({ _id: sinon.match.any })).to
            .be.true;
    });

    it("should delete a feedback", async () => {
        const mockId = "123456789012345678901234"; // A valid ObjectId string

        const mockDb = {
            feedbackCollection: {
                deleteOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.deleteFeedback(mockId);

        expect(result).to.deep.equal({});
        expect(mockDb.feedbackCollection.deleteOne.calledOnceWithExactly({ _id: sinon.match.any }))
            .to.be.true;
    });

    it("should update a feedback", async () => {
        const mockId = "123456789012345678901234"; // A valid ObjectId string
        const updatedFeedback = { title: "Updated Title", comment: "Updated Comment" };

        const mockDb = {
            feedbackCollection: {
                updateOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.updateFeedback(mockId, updatedFeedback);

        expect(result).to.deep.equal({});
        expect(
            mockDb.feedbackCollection.updateOne.calledOnceWithExactly(
                { _id: sinon.match.any },
                updatedFeedback,
            ),
        ).to.be.true;
    });

    it("should get all feedbacks by user", async () => {
        const mockUserName = "TestUser";
        const pageNumber = 1;
        const itemsPerPage = 10;

        const mockUser = {
            _id: new ObjectId("123456789012345678901234"),
        };

        const mockDb = {
            userCollection: {
                findOne: sandbox.stub().resolves(mockUser),
            },
            feedbackCollection: {
                find: sandbox.stub().returns({
                    skip: sandbox.stub().returns({
                        limit: sandbox.stub().returns({
                            toArray: sandbox
                                .stub()
                                .resolves([{ title: "Feedback1" }, { title: "Feedback2" }]),
                        }),
                    }),
                }),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getAllFeedbacksByUser(
            mockUserName,
            pageNumber,
            itemsPerPage,
        );

        expect(result).to.deep.equal([{ title: "Feedback1" }, { title: "Feedback2" }]);
        expect(mockDb.userCollection.findOne.calledOnceWithExactly({ name: mockUserName })).to.be
            .true;
        expect(mockDb.feedbackCollection.find.calledOnceWithExactly({ user_id: mockUser._id })).to
            .be.true;
        expect(
            mockDb.feedbackCollection
                .find()
                .skip.calledOnceWithExactly((pageNumber - 1) * itemsPerPage),
        ).to.be.true;
        expect(mockDb.feedbackCollection.find().skip().limit.calledOnceWithExactly(itemsPerPage)).to
            .be.true;
    });
});
