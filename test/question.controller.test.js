const { expect } = require("chai");
const sinon = require("sinon");
const { ObjectId } = require("mongodb");

const mockController = (() => {
    let mockDb = {
        questionCollection: null,
        categoryCollection: null,
    };

    const setMockDb = (db) => {
        mockDb.questionCollection = db.questionCollection;
        mockDb.categoryCollection = db.categoryCollection;
    };

    const createQuestion = async (question) => {
        if (!question || !question.text) {
            throw new Error("Invalid question data");
        }

        question.views = 0;
        let categories_ids = [];
        for (let category of question.categories) {
            categories_ids.push(new ObjectId(category));
        }

        if (!question.fact) {
            question.fact =
                "TokenTrivia is an online trivia game that rewards using crypto. It is a fun and easy way to engage in the web3 ecosystem.";
        } else {
            question.fact = question.fact;
        }

        question.categories = categories_ids;

        for (let category_id of question.categories) {
            const category = await mockDb.categoryCollection.findOne({ _id: category_id });
            if (!category) throw new Error("Category not found");
        }

        if (await mockDb.questionCollection.findOne({ text: question.text })) {
            throw new Error("The question is already in use");
        }

        const response = await mockDb.questionCollection.insertOne(question);
        return response;
    };

    const getQuestion = async (id) => {
        const response = await mockDb.questionCollection.findOne({ _id: new ObjectId(id) });
        return response;
    };

    const getRandomQuestionsByCategory = async (category_id, count) => {
        const pipeline = [
            { $unwind: "$categories" },
            {
                $match: {
                    $and: [{ categories: new ObjectId(category_id) }, { difficulty: 0 }],
                },
            },
            { $sample: { size: count } },
        ];

        const response = await mockDb.questionCollection.aggregate(pipeline).toArray();
        return response;
    };

    const getRandomQuestions = async (count) => {
        const pipeline = [{ $match: { difficulty: 0 } }, { $sample: { size: count } }];

        const response = await mockDb.questionCollection.aggregate(pipeline).toArray();
        return response;
    };

    return {
        setMockDb,
        createQuestion,
        getQuestion,
        getRandomQuestionsByCategory,
        getRandomQuestions,
    };
})();

describe("Question Controller", () => {
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should create a question", async () => {
        const mockQuestion = {
            text: "Test Question",
            categories: ["123456789012345678901234", "234567890123456789012345"],
        };

        const mockDb = {
            questionCollection: {
                findOne: sandbox.stub(),
                insertOne: sandbox.stub().resolves({ insertedCount: 1 }),
            },
            categoryCollection: {
                findOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.createQuestion(mockQuestion);

        expect(result.insertedCount).to.equal(1);
        expect(mockDb.categoryCollection.findOne.calledTwice).to.be.true; // Check if findOne is called for each category
        expect(mockDb.questionCollection.insertOne.calledOnceWithExactly(sinon.match(mockQuestion)))
            .to.be.true;
    });

    it("should get a question by ID", async () => {
        const mockId = "123456789012345678901234"; // A valid ObjectId string

        const mockDb = {
            questionCollection: {
                findOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getQuestion(mockId);

        expect(result).to.deep.equal({});
        expect(mockDb.questionCollection.findOne.calledOnceWithExactly({ _id: sinon.match.any })).to
            .be.true;
    });

    it("should get random questions by category", async () => {
        const mockCategoryId = "123456789012345678901234"; // A valid ObjectId string
        const mockCount = 5;

        const mockDb = {
            questionCollection: {
                aggregate: sandbox.stub().returns({
                    toArray: sandbox
                        .stub()
                        .resolves([{ text: "Question1" }, { text: "Question2" }]),
                }),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getRandomQuestionsByCategory(mockCategoryId, mockCount);

        expect(result).to.deep.equal([{ text: "Question1" }, { text: "Question2" }]);
        expect(mockDb.questionCollection.aggregate.calledOnce).to.be.true;
    });

    it("should get random questions", async () => {
        const mockCount = 5;

        const mockDb = {
            questionCollection: {
                aggregate: sandbox.stub().returns({
                    toArray: sandbox
                        .stub()
                        .resolves([{ text: "Question1" }, { text: "Question2" }]),
                }),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getRandomQuestions(mockCount);

        expect(result).to.deep.equal([{ text: "Question1" }, { text: "Question2" }]);
        expect(mockDb.questionCollection.aggregate.calledOnce).to.be.true;
    });

    // You can add more tests or adapt these examples based on your specific requirements
});
