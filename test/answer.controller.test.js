const { expect } = require("chai");
const sinon = require("sinon");
const { ObjectId } = require("mongodb");

const mockController = (() => {
    let mockDb = {
        answerCollection: null,
        questionCollection: null,
    };

    const setMockDb = (db) => {
        mockDb.answerCollection = db.answerCollection;
        mockDb.questionCollection = db.questionCollection;
    };

    const createAnswer = async (answer) => {
        if (!answer || !answer.text || !answer.question_id) {
            throw new Error("Invalid answer data");
        }

        if (!answer.is_correct) {
            answer.is_correct = false;
        }

        const existingAnswer = await mockDb.answerCollection.findOne({ text: answer.text });
        if (existingAnswer) {
            throw new Error("The answer is already in use");
        }

        const question = await mockDb.questionCollection.findOne({
            _id: new ObjectId(answer.question_id),
        });

        if (!question) {
            throw new Error("Question not found");
        }

        answer.question_id = new ObjectId(answer.question_id);
        const result = await mockDb.answerCollection.insertOne(answer);
        return result;
    };

    const getAnswer = async (id) => {
        if (!id) {
            throw new Error("Invalid answer data");
        }

        const response = await mockDb.answerCollection.findOne({ _id: new ObjectId(id) });
        return response;
    };

    const getAnswerForQuestion = async (question_id) => {
        if (!question_id) {
            throw new Error("Invalid question data");
        }

        const answers = await mockDb.answerCollection
            .find({ question_id: new ObjectId(question_id) })
            .toArray();

        // Implement the shuffling logic here if needed
        // ...

        return answers;
    };

    return { setMockDb, createAnswer, getAnswer, getAnswerForQuestion };
})();

describe("Answer Controller", () => {
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should create an answer", async () => {
        const mockAnswer = {
            text: "Test Answer",
            question_id: "123456789012345678901234", // A valid ObjectId string
        };

        const mockQuestion = {
            _id: new ObjectId(mockAnswer.question_id),
        };

        const mockDb = {
            answerCollection: {
                findOne: sandbox.stub(),
                insertOne: sandbox.stub().resolves({}),
            },
            questionCollection: {
                findOne: sandbox.stub().resolves(mockQuestion),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.createAnswer(mockAnswer);

        expect(result).to.not.be.undefined;
        expect(mockDb.answerCollection.findOne.calledOnceWithExactly({ text: mockAnswer.text })).to
            .be.true;
        expect(
            mockDb.questionCollection.findOne.calledOnceWithExactly({
                _id: new ObjectId(mockAnswer.question_id),
            }),
        ).to.be.true;
        expect(
            mockDb.answerCollection.insertOne.calledOnceWithExactly(
                sinon.match({
                    text: mockAnswer.text,
                    question_id: new ObjectId(mockAnswer.question_id),
                    is_correct: false,
                }),
            ),
        ).to.be.true;
    });

    it("should get an answer", async () => {
        const mockId = "123456789012345678901234"; // A valid ObjectId string

        const mockDb = {
            answerCollection: {
                findOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getAnswer(mockId);

        expect(result).to.deep.equal({});
        expect(mockDb.answerCollection.findOne.calledOnceWithExactly({ _id: sinon.match.any })).to
            .be.true;
    });

    it("should get answers for a question", async () => {
        const mockQuestionId = "123456789012345678901234"; // A valid ObjectId string

        const mockDb = {
            answerCollection: {
                find: sandbox.stub().returns({
                    toArray: sandbox.stub().resolves([{ text: "Answer1" }, { text: "Answer2" }]),
                }),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getAnswerForQuestion(mockQuestionId);

        expect(result).to.deep.equal([{ text: "Answer1" }, { text: "Answer2" }]);
        expect(
            mockDb.answerCollection.find.calledOnceWithExactly({
                question_id: new ObjectId(mockQuestionId),
            }),
        ).to.be.true;
        expect(mockDb.answerCollection.find().toArray.calledOnce).to.be.true;
    });

    // Add similar tests for other functions as needed
});
