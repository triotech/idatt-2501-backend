const chai = require("chai");
const { hashString, compareString } = require("../utils/encryption");

const expect = chai.expect;

describe("Utility Functions", () => {
    describe("hashString", () => {
        it("should hash a string", async () => {
            const password = "testpassword";
            const hashedPassword = await hashString(password);
            expect(hashedPassword).to.be.a("string");
            expect(hashedPassword).to.not.equal(password);
        });
    });

    describe("compareString", () => {
        it("should compare a string with its hash and return true for a matching pair", async () => {
            const password = "testpassword";
            const hashedPassword = await hashString(password);
            const result = await compareString(password, hashedPassword);
            expect(result).to.equal(true);
        });

        it("should compare a string with its hash and return false for a non-matching pair", async () => {
            const password1 = "testpassword";
            const password2 = "anotherpassword";
            const hashedPassword = await hashString(password1);
            const result = await compareString(password2, hashedPassword);
            expect(result).to.equal(false);
        });
    });
});
