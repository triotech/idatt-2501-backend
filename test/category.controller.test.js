const { expect } = require("chai");
const sinon = require("sinon");
const { ObjectId } = require("mongodb");

const mockController = (() => {
    let mockDb = {
        categoryCollection: null,
    };

    const setMockDb = (db) => {
        mockDb.categoryCollection = db.categoryCollection;
    };

    const createCategory = async (category) => {
        if (!category || !category.name || !category.description) {
            throw new Error("Invalid category data");
        }

        if (await mockDb.categoryCollection.findOne({ name: category.name })) {
            throw new Error("The category is already in use");
        }

        const result = await mockDb.categoryCollection.insertOne(category);
        if (result.insertedCount === 0) {
            throw new Error("Category not created");
        }
        return true;
    };

    const getCategory = async (id) => {
        const response = await mockDb.categoryCollection.findOne({ _id: new ObjectId(id) });
        return response;
    };

    const getAllCategories = async () => {
        const response = await mockDb.categoryCollection.find({}).toArray();
        return response;
    };

    const getRandomCategory = async () => {
        const response = await mockDb.categoryCollection
            .aggregate([{ $sample: { size: 1 } }])
            .toArray();
        return response[0];
    };

    return { setMockDb, createCategory, getCategory, getAllCategories, getRandomCategory };
})();

describe("Category Controller", () => {
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should create a category", async () => {
        const mockCategory = {
            name: "Test Category",
            description: "Test description",
        };

        const mockDb = {
            categoryCollection: {
                findOne: sandbox.stub(),
                insertOne: sandbox.stub().resolves({ insertedCount: 1 }),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.createCategory(mockCategory);

        expect(result).to.be.true;
        expect(mockDb.categoryCollection.findOne.calledOnceWithExactly({ name: mockCategory.name }))
            .to.be.true;
        expect(mockDb.categoryCollection.insertOne.calledOnceWithExactly(mockCategory)).to.be.true;
    });

    it("should get a category by ID", async () => {
        const mockId = "123456789012345678901234";

        const mockDb = {
            categoryCollection: {
                findOne: sandbox.stub().resolves({}),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getCategory(mockId);

        expect(result).to.deep.equal({});
        expect(mockDb.categoryCollection.findOne.calledOnceWithExactly({ _id: sinon.match.any })).to
            .be.true;
    });

    it("should get all categories", async () => {
        const mockDb = {
            categoryCollection: {
                find: sandbox.stub().returns({
                    toArray: sandbox
                        .stub()
                        .resolves([{ name: "Category1" }, { name: "Category2" }]),
                }),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getAllCategories();

        expect(result).to.deep.equal([{ name: "Category1" }, { name: "Category2" }]);
        expect(mockDb.categoryCollection.find.calledOnceWithExactly({})).to.be.true;
    });

    it("should get a random category", async () => {
        const mockDb = {
            categoryCollection: {
                aggregate: sandbox.stub().returns({
                    toArray: sandbox.stub().resolves([{ name: "RandomCategory" }]),
                }),
            },
        };

        mockController.setMockDb(mockDb);
        sandbox.stub(require("../utils/mongoDB"), "connectToDatabase").resolves(mockDb);

        const result = await mockController.getRandomCategory();

        expect(result).to.deep.equal({ name: "RandomCategory" });
        expect(
            mockDb.categoryCollection.aggregate.calledOnceWithExactly([{ $sample: { size: 1 } }]),
        ).to.be.true;
    });
});
