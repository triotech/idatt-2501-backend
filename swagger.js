const swaggerJsdoc = require('swagger-jsdoc');

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'TokenTrivia API-DOCUMENTATION',
      version: '1.0.0',
      description: 'The official API documentation for the TokenTrivia application',
    },
    servers: [
      {
        url: 'http://localhost:8080/',
      },
    ],
  },
  apis: ['./routes/*.router.js'],
};

const specs = swaggerJsdoc(options);

module.exports = specs;
